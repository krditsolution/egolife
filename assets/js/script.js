$(document).ready(function () {
  // let oldValue = 0;
  let siteHeader = $(".site-header");
  // let siteHeaderHeight = siteHeader.height();
  // console.log(siteHeaderHeight);
  $(window).scroll(function (e) {
    //navbar shrink
    var siteHeader = $("#siteHeader");
    $(window).scroll(function () {
      if ($(document).scrollTop() > 50) {
        siteHeader.addClass("site-header--shrinked shadow");
      } else {
        siteHeader.removeClass("site-header--shrinked shadow");
      }
    });

    // Scroll Top fade in out
    if ($(document).scrollTop() > 200) {
      $(".scroll-to-top-button").addClass("scroll-to-top-button--show");
    } else {
      $(".scroll-to-top-button").removeClass("scroll-to-top-button--show");
    }

    // if ($(document).scrollTop() > 200) {
    //   // Get the new Value
    //   newValue = window.pageYOffset;

    //   //Subtract the two and conclude
    //   if (oldValue - newValue < 0) {
    //     siteHeader.addClass("site-header--shrinked");
    //   } else if (oldValue - newValue > 0) {
    //     siteHeader.removeClass("site-header--shrinked");
    //   }

    //   // Update the old value
    //   oldValue = newValue;
    // }
  });

  var swiperBestSeller = new Swiper(".best-seller-slider", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: true,
    pauseOnMouseEnter: true,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  var swiperSyncTabSlider = new Swiper(".sync-tab-products-slider", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: true,
    pauseOnMouseEnter: true,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  // Header Mobile Nav Dropdown Menu
  var navLinks = $(".site-header__mobile-nav > ul li a");
  navLinks.each(function () {
    $(this).next().first().slideUp(0);
  });
  navLinks.each(function () {
    $(this).on("click", function () {
      if ($(this).hasClass("has-dropdown")) {
        $(this).parent().siblings().find("ul").slideUp(300);
        $(this).parent().siblings().find("i").removeClass("fa-minus").addClass("fa-plus");
        $(this).next().first().slideToggle(300);
        if ($(this).find("i").hasClass("fa-plus")) {
          $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
        } else {
          $(this).find("i").removeClass("fa-minus").addClass("fa-plus");
        }
      }
    });
  });

  //Global Search Form select inputs
  $("#plSortByPrice").select2({
    theme: "bootstrap-5",
    minimumResultsForSearch: -1,
  });

  //Click event to scroll to top
  $(".scroll-to-top-button").click(function () {
    $("html, body").animate({ scrollTop: 0 }, 500);
    return false;
  });

  // adding active class to main navigation
  function dynamicCurrentMenuClass(selector) {
    var FileName = window.location.href.split("/").reverse()[0];

    selector.find("li").each(function () {
      var getAnchorHref = $(this).find("a").attr("href");

      if (getAnchorHref == FileName) {
        $(this).find("a").addClass("active");
      }
    });
    // if any li has .current elmnt add class
    selector.children("li").each(function () {
      if ($(this).find(".active").length) {
        $(this).find("a").addClass("active");
      }
    });
    // if no file name return
    if ("" == FileName) {
      selector.find("li").eq(0).find("a").addClass("active");
    }
  }

  // dynamic active class
  let mainNavUL = $(".site-header .site-header__nav ul");
  let mainMobileNavUL = $(".site-header .site-header__mobile-nav ul");
  dynamicCurrentMenuClass(mainNavUL);
  dynamicCurrentMenuClass(mainMobileNavUL);

  // show password button function
  var showPasswordButtons = $(".form .button--show-password");
  var showPassFlag = false;
  showPasswordButtons.each(function () {
    $(this).on("click", function (e) {
      e.preventDefault();
      if (!showPassFlag) {
        $(this).siblings("input").attr("type", "text");
        $(this).find("i").removeClass("bi-eye").addClass("bi-eye-slash");
        showPassFlag = true;
      } else {
        $(this).siblings("input").attr("type", "password");
        $(this).find("i").removeClass("bi-eye-slash").addClass("bi-eye");
        showPassFlag = false;
      }
    });
  });

  //Form material label control
  var formInputs = $(".form__input");
  formInputs.each(function () {
    $(this).on("focus", function () {
      $(this).prev().addClass("anim-label--shrinked");
    });
    $(this).on("blur", function () {
      if ($(this).val().length === 0) $(this).prev().removeClass("anim-label--shrinked");
    });
  });

  // Add address on cart page
  $(".address-container__button--add-adress").on("click", function () {
    console.log("clicked");
    $("body").addClass("bg-overlay");
  });
  var addAddressOffCanvas = $("#addAddressOffCanvas");
  addAddressOffCanvas.on("hidden.bs.offcanvas", function () {
    $("body").removeClass("bg-overlay");
  });

  // All packages sliders content
  var breakpointsDetails = {
    // when window width is >= 320px
    320: {
      slidesPerView: 2,
      spaceBetween: 8,
    },
    // when window width is >= 480px
    576: {
      slidesPerView: 2,
      spaceBetween: 16,
    },
    767: {
      slidesPerView: 3,
      spaceBetween: 16,
    },
    992: {
      slidesPerView: 4,
      spaceBetween: 16,
    },
    1200: {
      slidesPerView: 5,
      spaceBetween: 16,
    },
  };
  var paginationStructure = {
    el: ".swiper-pagination",
    clickable: true,
    renderCustom: function (swiper, current, total) {
      var names = [];
      $(".swiper-wrapper .swiper-slide").each(function (i) {
        names.push($(this).data("name"));
      });
      var text = "<ul>";
      for (let i = 1; i <= total; i++) {
        if (current == i) {
          text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
        } else {
          text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
        }
      }
      text += "</ul>";
      return text;
    },
  };
  var navigationArrowsHtml = {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  };
  var slider1 = new Swiper(".slider-1", {
    // Optional parameters
    direction: "horizontal",
    slidesPerView: 5,
    spaceBetween: 10,
    loop: true,
    autoplay: true,
    pauseOnMouseEnter: true,
    breakpoints: breakpointsDetails,
    // If we need pagination
    pagination: paginationStructure,

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next-1",
      prevEl: ".swiper-button-prev-1",
    },
  });

  // Testimonial Swiper
  var swiperTestimonial = new Swiper(".swiper-testimonial", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: {
      delay: 5000,
      pauseOnMouseEnter: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 1,
        spaceBetween: 15,
      },
      992: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      1200: {
        slidesPerView: 2,
        spaceBetween: 50,
      },
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next--testimonial",
      prevEl: ".swiper-button-prev--testimonial",
    },
  });
  //Trending swiper
  var swiperTrending = new Swiper(".swiper-announement", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: {
      delay: 5000,
      pauseOnMouseEnter: true,
    },
    breakpoints: {
      768: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 15,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 15,
      },
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next--announcement",
      prevEl: ".swiper-button-prev--announcement",
    },
  });

  // Products Page sub category dropdown menu
  var catItemExpandBtns = $(".category-filter-list .category-filter__item .item__button--expand");

  catItemExpandBtns.each(function () {
    $(this).parent().next().slideUp(0);
    $(this).on("click", function () {
      if (!$(this).hasClass("expanded")) {
        $(".category-filter-list .category-filter__item .item__dropdown-menu").slideUp();
        $(".category-filter-list .category-filter__item .item__button--expand").removeClass("expanded");
        $(this).addClass("expanded");
        $(this).parent().next().slideDown();
      } else {
        $(this).removeClass("expanded");
        $(this).parent().next().slideUp();
      }
    });
  });
  var checkboxes = $(".category-filter__item input.subOption"),
    checkall = $(".category-filter__item .mainOption");

  checkall.each(function () {
    var checkedFlag = false;
    $(this).on("click", function () {
      if (!checkedFlag) {
        $(this).prop("checked", true);
        $(this).parent().parent().find(".subOption").prop("checked", true);
        checkedFlag = true;
      } else {
        $(this).prop("checked", false);
        $(this).parent().parent().find(".subOption").prop("checked", false);
        checkedFlag = false;
      }
    });
  });

  // Product single page accordion & Faqs page accordion
  $(".set > a").on("click", function () {
    if ($(this).hasClass("active")) {
      $(this).removeClass("active");
      $(this).siblings(".content").slideUp(200);
      $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
    } else {
      $(".set > a i").removeClass("fa-minus").addClass("fa-plus");
      $(this).find("i").removeClass("fa-plus").addClass("fa-minus");
      $(".set > a").removeClass("active");
      $(this).addClass("active");
      $(".content").slideUp(200);
      $(this).siblings(".content").slideDown(200);
    }
  });

  // Sync Tab section
  var syncTabBtn = $(".tab-btns-wrapper .tab-btn");

  var thumbnailItems = $(".sync-products-tab-section .thumbnail-list .thumbnail-list__item");
  var categoryListItems = $(".sync-products-tab-section .category-list .category-list__item");
  var categorySliders = $(".sync-products-tab-section .sync-tab-products-slider");
  var counterVal = 0;
  var selectedCategory = "";

  syncTabBtn.on("click", function () {
    // Prev Button function
    if ($(this).attr("id") === "syncTabBtnPrev") {
      if (counterVal == 0) {
        counterVal = categoryListItems.length - 1;
        listItemControl(thumbnailItems, counterVal);
        listItemControl(categoryListItems, counterVal);
        updateCatSlider();
      } else {
        counterVal--;
        listItemControl(thumbnailItems, counterVal);
        listItemControl(categoryListItems, counterVal);
        updateCatSlider();
      }
    }

    // Next Button Function
    if ($(this).attr("id") === "syncTabBtnNext") {
      if (counterVal == categoryListItems.length - 1) {
        counterVal = 0;
        listItemControl(thumbnailItems, counterVal);
        listItemControl(categoryListItems, counterVal);
        updateCatSlider();
      } else {
        counterVal++;
        listItemControl(thumbnailItems, counterVal);
        listItemControl(categoryListItems, counterVal);
        updateCatSlider();
      }
    }
  });

  function listItemControl(list, ctVal) {
    $.each(list, function (idx, val) {
      if (ctVal == idx) {
        $(this).addClass("active");
      } else {
        $(this).removeClass("active");
      }
    });
  }

  function updateCatSlider() {
    categoryListItems.each(function () {
      if ($(this).hasClass("active")) {
        selectedCategory = $(this).attr("data-category");
        categorySliders.each(function () {
          if ($(this).attr("data-category") === selectedCategory) {
            $(this).addClass("active");
          } else {
            $(this).removeClass("active");
          }
        });
      }
    });
  }

  // Filters Toggling on Proudcts Grid Page
  var filterTogglerBtn = $(".products-grid-section .products-grid__btn--filter-toggler");
  var filterWrapperEl = $(".products-grid-section .filters-wrapper");
  isFiltersActive = false;
  filterTogglerBtn.on("click", function () {
    if (!isFiltersActive) {
      filterWrapperEl.slideDown(300);
      isFiltersActive = true;
    } else {
      filterWrapperEl.slideUp(300);
      isFiltersActive = false;
    }
  });

  // quanity increment and decrement funciton
  var qtyPlusBtns = $(".qtyplus");
  var qtyMinusBtns = $(".qtyminus");

  qtyPlusBtns.each(function () {
    $(this).on("click", function () {
      var getVal = $(this).prev().val();
      getVal = parseInt(getVal);
      // console.log(typeof getVal);
      $(this)
        .prev()
        .attr("value", getVal + 1);
    });
  });

  qtyMinusBtns.each(function () {
    $(this).on("click", function () {
      console.log("clicked");
      var getVal = $(this).next().val();
      getVal = parseInt(getVal);
      if (getVal > 1) {
        $(this)
          .next()
          .attr("value", getVal - 1);
      }
    });
  });

  // search form function in header
  var searchTriggerBtn = $(".site-header .site-header__button--search");
  var searchFormContainerEl = $(".site-header .search-form-container");
  var searchFormContainerBtnClose = $(".site-header .search-form-container .search-form-container__btn--close");
  var isSearchFormOpened = false;
  searchTriggerBtn.on("click", function () {
    if (!isSearchFormOpened) {
      console.log("clicked");
      searchFormContainerEl.addClass("search-form-container--shown");
      isSearchFormOpened = true;
    }
  });

  searchFormContainerBtnClose.on("click", function () {
    searchFormContainerEl.removeClass("search-form-container--shown");
    isSearchFormOpened = false;
  });

  // Custom Dropdown menu
  var dropdownMenuBtnEl = $(".custom-dropdown-btn");
  var isAnyDropdownMenuActive = false;
  dropdownMenuBtnEl.each(function () {
    $(this).on("click", function () {
      var getTarget = $(this).attr("data-target");
      if (!isAnyDropdownMenuActive) {
        $("#" + getTarget + "").addClass("custom-dropdown-menu--show");
        isAnyDropdownMenuActive = true;
      } else {
        $("#" + getTarget + "").removeClass("custom-dropdown-menu--show");
        isAnyDropdownMenuActive = false;
      }
    });
  });

  $("html").click(function (event) {
    if ($(event.target).closest(".custom-dropdown-menu, .custom-dropdown-btn").length === 0) {
      if (isAnyDropdownMenuActive) {
        $(".custom-dropdown-menu").removeClass("custom-dropdown-menu--show");
      }
    }
  });

  // Color variation buttons function on product single page
  var productSingleColorVarBtns = $(".product-single__color-wrapper a");
  productSingleColorVarBtns.each(function () {
    $(this).on("click", function () {
      // getting the color attribute name
      var getColorAttName = $(this).attr("data-color");
      // console.log(getColorAttName);
      $(".product-single__color-wrapper a").removeClass("active");
      $(this).addClass("active");
    });
  });

  // Product single page swiper gallery
  var swiper = new Swiper(".product-single-swiper-thumb", {
    direction: "vertical",
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesProgress: true,
  });
  var swiper2 = new Swiper(".product-single-swiper-view", {
    spaceBetween: 10,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    thumbs: {
      swiper: swiper,
    },
  });
  $(".btn--thumbnails-mp").on("click", function () {
    $(".product-single__magnific-container")
      .magnificPopup({
        delegate: "a",
        type: "image",
        gallery: {
          enabled: true,
        },
      })
      .magnificPopup("open");
  });

  // getting the data of price select - low to high or high to low - products page
  $("#plSortByPrice").on("select2:select", function (e) {
    var data = e.params.data;
    console.log(data);
    console.log(data.id);
  });

  // hero section swiper
  var swiperHero = new Swiper(".hero-swiper", {
    // Optional parameters
    direction: "horizontal",
    loop: true,
    autoplay: true,
    pauseOnMouseEnter: true,
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  // Products Slider
  // Categories V2 Slider
  var swiperProducts = new Swiper(".products-slider", {
    // Optional parameters
    direction: "horizontal",
    slidesPerView: 5,
    spaceBetween: 12,
    loop: true,
    autoplay: false,
    pauseOnMouseEnter: true,
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: false,
      },
      // when window width is >= 480px
      576: {
        slidesPerView: 2,
        spaceBetween: 24,
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 24,
      },
      992: {
        slidesPerView: 3,
        spaceBetween: 24,
      },
      1200: {
        slidesPerView: 4,
        spaceBetween: 24,
      },
    },
    // If we need pagination
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
      renderCustom: function (swiper, current, total) {
        var names = [];
        $(".swiper-wrapper .swiper-slide").each(function (i) {
          names.push($(this).data("name"));
        });
        var text = "<ul>";
        for (let i = 1; i <= total; i++) {
          if (current == i) {
            text += `<li class="swiper-pagination-bullet active">${names[i]}</li>`;
          } else {
            text += `<li class="swiper-pagination-bullet">${names[i]}</li>`;
          }
        }
        text += "</ul>";
        return text;
      },
    },

    // Navigation arrows
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  // megamenu
  var allCatBtn = $(".has-megamenu");
  isMegamenuShowing = false;
  // For single megamenu show hide function
  allCatBtn.on("mouseenter", function () {
    $(this).closest("ul").find(".mega-menu").addClass("show");
    isMegamenuShowing = true;
  });

  $("html").mouseover(function (event) {
    if (isMegamenuShowing) {
      if ($(event.target).closest(".site-header .mega-menu, .site-header .has-megamenu").length === 0) {
        $(".site-header .mega-menu").removeClass("show");
        isMegamenuShowing = false;
      }
    }
  });

  // Counter up
  // $(".counter").counterUp({
  //   delay: 10,
  //   time: 1000,
  // });
});
