<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    class Admin_model extends CI_Model{

        public function admin_login($data){
			$this->db->where($data);
			$user = $this->db->get('users');
			if($user->num_rows() > 0){
				return array('user_type' => $user->row()->user_type, 'user_id' => $user->row()->id);
			}else{
				false;
			}
        }

		public function get_all_users(){
			return $this->db->where('status', 1)
							->get('users')->result_array();
		}

		public function get_places(){
			return $this->db->where('status', 1)
							->where('user_type', 'Seller')
							->get('users')->result_array();
		}

		public function block_unblock_user($id, $data){
			$this->db->where('id', $id)
						->update('users', $data);
		}

    }
?>