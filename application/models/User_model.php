<?php 
    class User_model extends CI_model{
        public function getAllCourses(){
            return $this->db->get('products')->result_array();
        }

        public function getAllCoursesById($id){
            return $this->db->where('id', $id)
                        ->get('products')->result_array();
        }

        public function userLoginCheck($data){
            $user = $this->db->where('email_addr', $data['email_addr'])
                        ->where('password', $data['password'])
                        ->get('users');
            if($user->num_rows() > 0){
                return array('user' => $user->row()->user_type, 'user_id' => $user->row()->id);
            }else{
                false;
            }
        }

        public function get_product_details_by_id($id){
            return $this->db->where('id', $id)
                            ->get('products')->result_array();

        }

        public function order_Complete($data){
            $this->db->insert('orders', $data);

        }

        public function get_user_by_id($user_id){
            return $this->db->where('id', $user_id)
                            ->get('users')->result_array();

        }

        public function get_orders_by_id($user_id){
            return $this->db->where('user_id', $user_id)
                            -> order_by('id', 'DESC')   
                            ->get('orders')->result_array();

        }

        public function add_user($data){
            $this->db->insert('users', $data);
        }

        public function get_order_details_by_id($id){
            return $this->db->query("SELECT *,orders.id AS order_id, orders.product_id AS product_id FROM orders LEFT JOIN products ON products.id = orders.product_id WHERE orders.id = $id")->result_array();
        }

        public function check_exits_user($email_id){
            return $this->db->where('email_addr', $email_id)
                        ->where('user_type', "User")
                        ->get('users')->num_rows();
        }

        public function login_user($data){
            $user = $this->db->where($data)
			                ->get('users');
            if($user->num_rows() > 0){
				return array('user_type' => $user->row()->user_type, 'user_id' => $user->row()->id);
			}else{
				false;
			}
        }



        public function get_data_from_wishlist_by_user_id($id){
            return $this->db->query("SELECT * , products.id as product_id FROM products LEFT JOIN fevorite ON products.id = fevorite.product_id where fevorite.user_id =$id")->result_array();
        }

        public function get_data_from_cart_by_user_id($id){
            return $this->db->query("SELECT * , products.id as product_id FROM products LEFT JOIN cart ON products.id = cart.product_id where cart.user_id =$id")->result_array();
        }

        public function get_data_from_cart_by_session_id($id){
            return $this->db->query("SELECT * , products.id as product_id FROM products LEFT JOIN cart ON products.id = cart.product_id where cart.session_id =$id")->result_array();
        }

        public function get_product_by_product_id($id){
            return $this->db->where('product_id', $id)
                            ->get('fevorite')->result_array();
        }

        public function delete_cart($id){
            $this->db->where('id', $id)
                    ->delete('cart');
        }

        public function deleteWishProduct($id){
            $this->db->where('id', $id)
                    ->delete('fevorite');
        }

        public function deleteAllWishProduct($id){
            $this->db->where('user_id', $id)
                    ->delete('fevorite');
        }

        public function delete_all_cart($id){
            $this->db->where('user_id', $id)
                    ->delete('cart');
        }

        public function check_cart_by_user_id($product_id, $user_id){
            return $this->db->where('user_id', $user_id)
                    ->where('product_id', $product_id)
                    ->get('cart')->num_rows();
        }

        public function check_cart_by_session_id($product_id, $session_id){
            return $this->db->where('session_id', $session_id)
                    ->where('product_id', $product_id)
                    ->get('cart')->num_rows();
        }

        public function check_wishlist_by_user_id($product_id, $user_id){
            return $this->db->where('user_id', $user_id)
                    ->where('product_id', $product_id)
                    ->get('fevorite')->num_rows();
        }

        public function add_address_by_user($data){
            $this->db->insert('address', $data);
        }

        public function check_default_address($id){
            return $this->db->where('user_id', $id)
                            ->get('address')->num_rows();
        }

        public function get_last_address_id($id){
            return $this->db->where('user_id', $id)
                            ->where('is_default', 1)
                            ->order_by('id', 'DESC')
                            ->limit(1)
                            ->get('address')->result_array();
        }

        public function update_address_in_user_table($id, $update_address){
            $this->db->where('id', $id)
                    ->update('users', $update_address);
        }

        public function get_dat_from_tax_by_state($state){
            return $this->db->where('state', $state)
                            ->get('tax')->result_array();
        }

        public function get_address_by_user_id($user_id){
            return $this->db->where('user_id', $user_id)
                            ->get('address')->result_array();
        }

        public function get_default_address_by_user_id($user_id){
            return $this->db->where('user_id', $user_id)
                            ->where('is_default', 1)
                            ->get('address')->result_array();
        }

        public function insert_order_table($table_name, $data){
            $this->db->insert($table_name, $data);
        }

        public function get_last_order_from_table(){
            return $this->db->query('SELECT * FROM orders ORDER BY ID DESC LIMIT 1')->result_array();
        }

        public function removeDefaultAddress($address_id, $user_id){
            $this->db->query("UPDATE `address` SET `is_default` = '0' WHERE `address`.`user_id` = $user_id");
        }
    }
?>