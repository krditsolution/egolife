<?php 
    class Common_model extends CI_Model{
        public function get_data_from_table($table_name){
            return $this->db->get($table_name)->result_array();
        }

        public function get_data_from_table_limit($table_name, $limit){
            return $this->db->order_by('id','DESC')
                            ->limit($limit)
                            ->get($table_name)->result_array();
        }
        
        public function delete_data_from_table($table_name, $id){
            $this->db->where('id', $id)
                    ->delete($table_name);
        }

        public function get_data_from_table_by_id($table_name, $id){
            return $this->db->where('id', $id)
                            ->get($table_name)->result_array();
        }

        public function update_table($table_name, $id, $data){
            $this->db->where('id', $id)
                        ->update($table_name, $data);
        }
        
        public function add_data_table($table_name, $data){
            $this->db->insert($table_name, $data);
        }

        public function get_related_product($table_name, $limit, $condition){
            return $this->db->where($condition)
                            ->limit($limit)
                            ->get($table_name)->result_array();
        }
    }
    
?>