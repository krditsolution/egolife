<?php $this->load->view('template/header'); ?>

<?php
// $CI =& get_instance();
// $get_data_by_category_40 = $CI->Product_model->get_productBy_category_40();
// $get_data_by_category_39 = $CI->Product_model->get_productBy_category_39();
// $get_data_by_category_38 = $CI->Product_model->get_productBy_category_38();
// $get_data_by_latest_6 = $CI->Product_model->get_productBy_latest_6();



// $get_product_best_sale = $CI->Product_model->get_data_from_table_short_by();
// $get_product_table_mat = $CI->Product_model->get_data_from_table_by_category_limit(1);
// $fridge_mat = $CI->Product_model->get_data_from_table_by_category_limit(17);

?>

<main class="site-content site-content--home">
    <section class="hero-section">
        <div class="swiper hero-swiper h-100">
            <div class="swiper-wrapper">
                <div class="swiper-slide w-100 h-100 position-relative bg-dark">
                    <div class="slide-bg w-100 h-100 position-absolute top-0 left-0"><img src="<?php echo base_url() ?>assets/images/banner-1.webp" alt="" class="d-block w-100 h-100 fit-cover"></div>
                    <div class="container position-relative h-100">
                        <div class="row h-100 align-items-center">
                            <div class="col-12 col-lg-12 text-center">
                                <h3 class="text-white mb-1 mb-lg-0">We strives for</h3>
                                <h1 class="text-uppercase mb-1 mb-lg-0">excellence</h1>
                                <h3 class="text-white mb-2">in the information technology (IT)</h3>
                                <div class="pt-3"><a href="#" class="btn btn-primary">Request A Quote</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-slide w-100 h-100 position-relative bg-dark">
                    <div class="slide-bg w-100 h-100 position-absolute top-0 left-0"><img src="<?php echo base_url() ?>assets/images/banner-2.webp" alt="" class="d-block w-100 h-100 fit-cover"></div>
                    <div class="container position-relative h-100">
                        <div class="row h-100 align-items-center">
                            <div class="col-12 col-lg-12 text-center">
                                <h3 class="text-white mb-1 mb-lg-0">We are</h3>
                                <h1 class="text-uppercase mb-1 mb-lg-0">professionals</h1>
                                <h3 class="text-white mb-2">who have years of expertise in the IT business</h3>
                                <div class="pt-3"><a href="#" class="btn btn-primary">Request A Quote</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
    </section>
    <section class="welcome-section section--py">
        <div class="container">
            <div class="row g-xl-4 g-xxl-5 align-items-center">
                <div class="col-12 col-lg-1 welcome-block">
                    <h2 class="text-uppercase welcome-block__title fw-bold text-dark">Welcome</h2>
                </div>
                <div class="col-12 col-lg-6 mb-4 mb-lg-0 text-center text-lg-start">
                    <h3 class="text-dark mb-3 mb-lg-4">We are a company with its headquarters in Assam, that strives for excellence in the information technology (IT) industry by offering a variety of IT accessory supplies.</h3>
                    <p class="text-para mb-4">This includes e-Government, e-commerce, tax consulting, government procurement, IT-enabled services, manufacturing, construction, and government project work. Our team is made up of professionals who are extremely driven and have years of expertise in the IT business. We have succeeded to build a skilled, devoted, and enthusiastic staff that is aware of the needs of the market.</p>
                    <p class="mb-0"><a href="<?php echo base_url('about-us') ?>" class="btn btn-primary">Read More</a></p>
                </div>
                <div class="col-12 col-lg-5 pt-3 pt-lg-0">
                    <div class="welcome-grid d-grid">
                        <div class="grid-item grid-item--1"><img src="<?php echo base_url() ?>assets/images/welcome-img-1.webp" alt=""></div>
                        <div class="grid-item grid-item--2"><img src="<?php echo base_url() ?>assets/images/welcome-img-2.webp" alt=""></div>
                        <div class="grid-item grid-item--3"><img src="<?php echo base_url() ?>assets/images/welcome-img-3.webp" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section--py softwares-wrapper-section">
        <div class="container">
            <div class="section__header text-center text-md-start">
                <div class="row">
                    <div class="col-12 col-md-8 col-xl-7">
                        <h2 class="text-dark fw-bold text-uppercase">Services</h2>
                        <p class="text-para">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nostrum, mollitia.</p>
                    </div>
                    <div class="col-md-4 col-xl-5 text-end d-none d-md-block"><a href="<?php echo base_url('services') ?>" class="btn btn-primary d-inline-block">View All Services</a></div>
                </div>
            </div>
            <?php if (!empty($data['services'])) : ?>
                <div class="soft-items-grid">
                    <?php foreach ($data['services'] as $services) : ?>
                        <?php $image = json_decode($services['img'], true); ?>
                        <div class="soft-item service-card-container" data-id="<?php echo $services['id']; ?>">
                            <div class="soft-item__header"><img src="<?php echo base_url('assets/images/services/' . $image['images_0']) ?>" class="w-100 h-100 fit-cover" width="400" height="300" alt="Software Title"></div>
                            <div class="soft-item__body service-card">
                                <div class="soft-item__title line-clamp line-clamp-2" style="min-height: 0px;">
                                    <a target="_blank" style="font-size:16px" href="<?php echo $services['url'] ?>"><?php echo $services['name'] ?></a>
                                </div>
                                <div class="soft-item__cta-wrapper">
                                    <a href="#" class="service-card__btn--enquiry btn btn-primary soft-item__btn--download d-block w-100 text-center">Inquire Now<span class="ms-1"></span></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <div class="img__holder">
                    <img src="<?php echo base_url() ?>assets/images/no_data.webp" alt="" style="margin: 0 auto; height:220px;">
                    <h6 class="mb-0 text-danger soft-count--empty text-center">Sorry, No Software found.</h6>
                </div>
            <?php endif; ?>
        </div>
    </section>

    <section class="why-us-section section--py">
        <div class="container position-relative">
            <div class="row">
                <div class="col-12 col-lg-6 mb-4 mb-lg-0"><img src="<?php echo base_url() ?>assets/images/why-us-bg.webp" class="d-lg-none" alt="Why choose us" width="1920" height="1370"></div>
                <div class="col-12 col-lg-6 pt-3 pt-lg-0 ps-lg-5">
                    <h2 class="text-white fw-bold text-uppercase">Why Choose Us?</h2>
                    <p class="text-muted mb-4 mb-lg-5">We offer solutions whether they are time- or skill-related. Our goal is to strengthen the team, which transforms thoughts into deeds and dreams into reality. We have established ideals and are passionate about what we do.</p>
                    <div class="pt-2 pt-lg-0">
                        <div class="why-us-block bg-white">
                            <h4 class="why-us-block__title fw-semibold text-dark">Financial Strategy</h4>
                            <p class="why-us-block__desc mb-0">A finance strategy combines strategic and financial planning. The result is a functional roadmap that evaluates present assets, expenses, and budgets and fits them with the objectives of the business.</p>
                        </div>
                        <div class="why-us-block bg-white">
                            <h4 class="why-us-block__title fw-semibold text-dark">Business Bonding</h4>
                            <p class="why-us-block__desc mb-0">A guarantee of performance is necessary for many organizations, most frequently general contractors, temporary staffing agencies, window cleaning, and enterprises with government contracts, depending on the law or consumer demand.</p>
                        </div>
                        <div class="why-us-block bg-white">
                            <h4 class="why-us-block__title fw-semibold text-dark">Trust</h4>
                            <p class="why-us-block__desc mb-0">Without trust, the business cannot be done, influence is lost, leaders risk losing their teams, and salespeople risk losing their customers. The list continues. The center of any business is trust and connections, not money. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cta-featured-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 section--py">
                    <h1 class="text-dark fw-bold mb-4"><span class="text-primary">Consultation</span><br>is one click away!</h1>
                    <p class="text-para mb-4">Deals between a tax counselor and a firm looking to merge with or buy another company may be different from its professional relationship with an estate executor trying to reduce taxes on real estate. Based on the taxpayer's circumstances, a tax counselor may provide different advice and services.</p>
                    <div class="cta-btns-wrapper"><a href="#" data-bs-toggle="modal" data-bs-target="#reqAQuoteModal" class="btn btn-dark me-2 me-sm-3">Book Appointment</a>
                        <!-- <a href="#" class="btn btn-outline-primary">Watch Demo</a> -->
                    </div>
                </div>
                <div class="col-12 col-lg-6 bg-col"><img src="<?php echo base_url() ?>assets/images/tax-consult-demo.png" alt="" width="300" height="557"></div>
            </div>
        </div>
    </section>
    <?php echo testimonial() ?>
    <?php echo faq() ?>

</main>


<?php $this->load->view('/template/footer'); ?>