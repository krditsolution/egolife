<?php $this->load->view('template/header'); ?>
<main class="site-content site-content--about">
    <section class="pagetitle-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">About Us</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>About Us</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="welcome-section section--py">
        <div class="container">
            <div class="row g-xl-4 g-xxl-5 align-items-center">
                <div class="col-12 col-lg-1 welcome-block d-none">
                    <h2 class="text-uppercase welcome-block__title fw-bold">Welcome</h2>
                </div>
                <div class="col-12 col-lg-6 mb-4 mb-lg-0 text-center text-lg-start">
                    <h3 class="text-dark mb-3 mb-lg-4 d-none">Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse id ut vel sunt beatae quod, dolor earum veritatis quaerat architecto!</h3>
                    <p class="text-para mb-4">Egolife capital concentrates on enhancing your operating effectiveness, generating money, and meeting your objectives. Our team of professionals has successfully completed numerous projects and engagements with various businesses, giving them a broad portfolio and set of abilities.</p>
                </div>
                <div class="col-12 col-lg-6 pt-3 pt-lg-0">
                    <div class="welcome-grid d-grid">
                        <div class="grid-item grid-item--1"><img src="<?php echo base_url() ?>assets/images/welcome-img-1.webp" alt=""></div>
                        <div class="grid-item grid-item--2"><img src="<?php echo base_url() ?>assets/images/welcome-img-2.webp" alt=""></div>
                        <div class="grid-item grid-item--3"><img src="<?php echo base_url() ?>assets/images/welcome-img-3.webp" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vm-section section--py position-relative">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6 position-relative">
                    <div class="vm-wrapper bg-white">
                        <div class="vm-block vm-block--vision active p-4 p-lg-5 bg-white" data-category="vision">
                            <h2 class="text-dark text-uppercase mb-4">Our Vision<span class="ms-3 icon"><img src="<?php echo base_url() ?>assets/images/eye.png" alt=""></span></h2>
                            <p class="text-para">To become the worldwide leader and trusted company for subsequent-technology records IT solutions with extraordinary crew of IT technocrats allowing superior returns on clients’ generation investments via creative and extraordinary solution. To ensure excellence, we would go a step further. Build a better infrastructure for better work ethics and cater to emerging markets, gaining customer’s trust and serve best in the industries . We believe in building relationship and a world a better vicinity.</p><button class="btn btn-primary vm-block__btn--cta" data-target="mission">Read Our Mission<i class="fa-solid fa-arrow-right ms-2"></i></button>
                        </div>
                        <div class="vm-block vm-block--mission p-4 p-lg-5 bg-white" data-category="mission">
                            <h2 class="text-dark text-uppercase mb-4">Our Mission<span class="ms-3 icon"><img src="<?php echo base_url() ?>assets/images/mission.png" alt=""></span></h2>
                            <p class="text-para">Our mission is to resolve real world challenges by means of harnessing the use of technology. We will pursue innovation focused on simplifying lives providing aggressive benefit to our customers as the real world and IT world converges. In pursuing our mission, we are able to supply Cost Effective products & offerings, preserve sustainable boom, build together beneficial and enduring partnerships and create long term value through following sound commercial enterprise practices in dealing with our clients, partners, employees and shareholders. As a growing company, Egolife Egovernance Private Limited will pursue persistent self improvement, personal excellence and responsibility delivering better than anticipated business  outcomes to our clients – proper, the first time.</p><button class="btn btn-primary vm-block__btn--cta" data-target="vision"><i class="fa-solid fa-arrow-left me-2"></i>Read Our Vision</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="images-list position-absolute top-0 left-0 w-100 h-100"><img src="<?php echo base_url() ?>assets/images/vision.webp" alt="" class="w-100 h-100 fit-cover active" data-category="vision"> <img src="<?php echo base_url() ?>assets/images/mission.webp" alt="" class="w-100 h-100 fit-cover" data-category="mission"></div>
    </section>

    <?php echo testimonial()?>
    <?php echo faq()?>

    
</main>
<?php $this->load->view('template/footer'); ?>