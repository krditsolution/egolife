<?php $this->load->view('template/header'); ?>
<main class="site-content site-content--contact">
    <section class="pagetitle-section d-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">Contact</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>Contact</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="section--py contact-section position-relative">
        <div class="container position-relative">
            <div class="row">
                <div class="col-12 col-lg-6 mb-4">
                    <div class="section__header">
                        <h2 class="text-white fw-bold text-uppercase">Get In Touch</h2>
                        <p class="text-white">We enable you in sharing your shortcomings when you work with us and provide the best solutions.</p>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-8 col-xl-7">
                            <div class="contact-block">
                                <p class="text-primary fw-semibold mb-2">Email</p>
                                <h6 class="contact--link"><a href="mailto:info.egolifecapital.in">info.egolifecapital.in</a><br><a href="mailto:support.egolifecapital.in">support.egolifecapital.in</a></h6>
                            </div>
                            <div class="contact-block">
                                <p class="text-primary fw-semibold mb-2">Phone</p>
                                <h6 class="contact--link"><a href="tel:+91 9365336191">+91 9365336191</a></h6>
                            </div>
                            <div class="contact-block">
                                <p class="text-primary fw-semibold mb-2">Address</p>
                                <h6 class="contact--link"><a href="javascipt:void(0)">House no 188 1st Floor Shiksha Public School Building Panjabari Road Panjabari Guwahati-37</a></h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 pt-3 pt-lg-0 ps-lg-5">
                    <form action="#" class="form contact-form">
                        <div class="row">
                            <div class="form__field"><input name="cf_name" class="form__input" placeholder="Full Name*" required></div>
                            <div class="form__field"><input type="email" name="cf_email" class="form__input" placeholder="Email*" required></div>
                            <div class="form__field"><input name="cf_phone" class="form__input" placeholder="Phone*" required></div>
                            <div class="form__field">
                                <select name="cf_select_subject" class="form__input" id="cfSelectSubject" required>
                                    <option value="" disabled="disabled" selected="selected">Select Subject</option>
                                    <option value="service">Service Related</option>
                                    <option value="product">Product Related</option>
                                    <option value="others">Others</option>
                                </select>
                            </div>
                            <div class="form__field" style="display: none;">
                                <select name="cf_select_service" class="form__input" id="cfSelectService" required>
                                    <option value="" disabled="disabled" selected="selected">Select Service</option>

                                    <?php
                                    $CI = &get_instance();
                                    $ger_services = $CI->Common_model->get_data_from_table('services');
                                    ?>
                                    <?php foreach ($ger_services as $get_service) : ?>
                                        <option data-id="<?php print_r($get_service['id']); ?>" value="<?php print_r($get_service['name']); ?>"><?php print_r($get_service['name']); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <div class="form__field"><textarea name="cf_message" class="form__input" placeholder="Message(optional)"></textarea></div>
                            <div class="text-center"><button type="submit" class="btn btn-primary">Submit</button></div>
                        </div>
                    </form>
                </div>
            </div>
        </div><img src="<?php echo base_url('assets/images/shape-honeycomb.svg')?>" alt="" class="shape">
    </section>
</main>
<?php $this->load->view('template/footer'); ?>