<?php $this->load->view('template/header');?>

<main>
    <!-- page title -->
    <section class="pagetitle-section d-flex align-items-center justify-content-center mb-5">
        <div class="pagetitle-block py-4 border-top border-bottom">
            <h3 class="text-primary py-2 text-uppercase">Offer Zone</h3>
        </div>
    </section>
</main>
<?php $this->load->view('template/footer');?>