<?php $this->load->view('template/header') ?>
<main class="site-content site-content--services">
    <section class="pagetitle-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">Services</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>Services</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="services-section section--py">
        <div class="container">
            <?php if (!empty($data['services'])) : ?>
                <div class="services-cards-grid mb-2 mb-md-0 justify-content-start">
                    <?php foreach ($data['services'] as $services) : ?>
                        <?php $image = json_decode($services['img'], true); ?>
                        <div class="service-card-container">
                            <div class="service-card bg-white p-3">
                                <div class="soft-item__header ratio ratio-4x3">
                                    <img src="<?php echo base_url('assets/images/services/' . $image['images_0']) ?>" class="w-100 h-100 fit-cover" width="400" height="300" alt="Software Title">
                                </div>

                                <div class="service-card__body mt-3">
                                    <h4 class="service-card__title mb-2 fs-5">
                                        <a href="<?php echo $services['url'] ?>" target="_blank" class="dark-link fs-5"><?php echo $services['name'] ?></a>
                                    </h4>
                                    <div class="text-para line-clamp line-clamp-2"><?php echo $services['description']?></div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <div class="img__holder">
                    <img src="<?php echo base_url() ?>assets/images/no_data.webp" alt="" style="margin: 0 auto; height:220px;">
                    <h6 class="mb-0 text-danger soft-count--empty text-center">Sorry, No Software found.</h6>
                </div>
            <?php endif; ?>
        </div>
    </section>
    <?php echo faq();?>
</main>
<style>
    .product-item-main .product-item-main__header {
        border-radius: 5px !important;
        aspect-ratio: 33/31 !important;
        overflow: hidden !important;
    }

    .product-item-main .product-item-main__header .product-item-main__thumbnail-container {
        display: block !important;
        width: 100% !important;
        height: 100% !important;
    }

    .product-item-main .product-item-main__header .product-item-main__thumbnail-container img {
        width: 100%;
        height: 100%;
        -o-object-fit: cover;
        object-fit: cover;
        -webkit-transition: -webkit-transform .6s ease-in-out;
        transition: -webkit-transform .6s ease-in-out;
        -o-transition: transform .6s ease-in-out;
        transition: transform .6s ease-in-out;
        transition: transform .6s ease-in-out, -webkit-transform .6s ease-in-out;
    }
</style>
<?php $this->load->view('template/footer') ?>