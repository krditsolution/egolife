<?php $this->load->view('template/header');?>
<main class="site-content site-content--products">
    <section class="pagetitle-section d-flex align-items-center justify-content-center">
        <div class="pagetitle-block py-4 border-top border-bottom">
            <h3 class="text-primary py-2 text-uppercase"><?php echo $data['category'][0]['name']?></h3>
        </div>
    </section>
    <!-- Products Grid -->
    <div class="products-grid-section section--py">
        <div class="container">
            <div class="products-grid__top d-none">
                <div class="row justify-content-between justify-content-lg-end align-items-center">
                    <div class="col-6 d-lg-none">
                        <a href="javascript:void(0)" class="btn btn-secondary px-3 py-2 d-inline-block rounded text-sm products-grid__btn--filter-toggler"><i class="fa-solid fa-filter me-1"></i>Filter</a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 col-xl-2 d-none">
                        <select class="form-select" id="plSortByPrice" data-placeholder="Sort By Price">
                            <option></option>
                            <option>Low to High</option>
                            <option>High to Low</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-xl-12 col-xxl-12 pt-3 pt-xl-0">
                    <div class="products-grid__main">
                        <div class="products-grid mb-3">
                            <?php if(!empty($data['products'])):?>
                                <?php foreach ($data['products'] as $key => $data):?>
                                    <?php 
                                        $images = json_decode($data['img'], TRUE);
                                    ?>
                                    <div class="product-item-main">
                                        <div class="product-item-main__header">
                                            <a href="<?php echo base_url('product-details/'.$data['id'])?>" class="product-item-main__thumbnail-container">
                                                <img style="border-radius: 3px;" src="<?php echo base_url('assets/images/products/').$images['images_0']?>" alt="<?php echo "Viva ".$data['name']?>">
                                            </a>
                                        </div>
                                        <div class="product-item-main__body">
                                            <a class="product-item-main__title" href="<?php echo base_url('product-details/'.$data['id'])?>"><?php echo $data['name']?></a>
                                            <!-- <h4 class="product-item-main fw-semibold text-primary">
                                                <span class=""><i class="fa-solid fa-indian-rupee-sign"></i></span>
                                                <span class="mr-5"><?php echo $data['selling_price']?></span>

                                                <span class=""><i class="fa-solid fa-indian-rupee-sign"></i></span>
                                                <span class=""><?php echo $data['mrp']?></span>
                                            </h4> -->

                                            <div class="product-item-main__price-container d-flex align-items-center product-item-main fw-semibold ">
                                                <span class="sale-price text-primary">&#8377 <?php echo $data['selling_price']?></span>
                                                <span class="regular-price text-dark">&#8377 <?php echo $data['mrp']?></span>
                                            </div>

                                        </div>
                                    </div>
                                <?php endforeach;?>
                            <?php else:?>
                                <h3 class="text-danger text-center">No Data Found!</h3>
                            <?php endif;?>
                        </div>
                        <div class="d-flex justify-content-end pt-4">
                            <nav aria-label="Page navigation example" class="d-inline-block">
                                <ul class="pagination">
                                    <?php //echo $this->pagination->create_links(); ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $this->load->view('template/footer');?>