<?php $this->load->view('template/header');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
 

<main class="site-content site-content--products">
    <section class="pagetitle-section d-flex align-items-center justify-content-center">
        <div class="pagetitle-block py-4 border-top border-bottom">
            <h3 class="text-primary py-2 text-uppercase">Products</h3>
        </div>
    </section>
    <!-- Products Grid -->
    <div class="products-grid-section section--py">
        <div class="container">
            <div class="products-grid__top">
                <div class="row justify-content-between justify-content-lg-end align-items-center">
                    <div class="col-6 d-lg-none">
                        <a href="javascript:void(0)" class="btn btn-secondary px-3 py-2 d-inline-block rounded text-sm products-grid__btn--filter-toggler"><i class="fa-solid fa-filter me-1"></i>Filter</a>
                    </div>
                    <div class="col-6 col-md-4 col-lg-3 col-xl-2 d-none">
                        <select class="form-select" name="short_by" id="plSortByPrice" data-placeholder="Sort By Price">
                            <option></option>
                            <option value="1">Low to High</option>
                            <option value="0">High to Low</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-xl-4 col-xxl-3 mb-4 mb-xl-0 filters-wrapper">
                    <div class="products-grid__filter">
                        <div class="filter-item mb-4">
                            <h3 class="text-primary">Category</h3>
                            <?php foreach($category['category'] as $key => $category):?>
                                <div class="form-check">
                                    <input class="form-check-input common_selector brand" type="checkbox" value="<?php echo $category['id']?>" id="cateogory_title">
                                    <label class="form-check-label" for="flexCheckChecked">
                                        <?php print_r($category['name']);?>
                                    </label>
                                </div>
                            <?php endforeach;?>
                        </div>
                        <div class="filter-item filter-item--price-range pt-3">
                            <div class="range-slider">
                                <span class="rangeValues"></span>
                                <div class="d-flex justify-content-between align-items-center pb-3">
                                    <span class="rangeValues--min text-secondary fw-semibold h6" data-val="">0</span>
                                    <span class="rangeValues--max text-secondary fw-semibold h6" data-val="">5000</span>
                                </div>
                                <input value="0" min="0" max="10000" step="100" type="range">
                                <input value="10000" min="1" max="10000" step="0" type="range">
                            </div>
                            <div class="text-lg-end pt-4">
                                <a href="javascript:void(0)" class="btn btn-secondary px-3 py-2 rounded-pill d-inline-block" id="get_price">Apply</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-xl-8 col-xxl-9 pt-3 pt-xl-0">
                    <div class="products-grid__main">
                        <div class="products-grid mb-3 filter_data">
                            
                        </div>
                        <div class="d-flex justify-content-end mt-3 mt-5" id="pagination_link"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
    
<style>
    #loading {
        text-align: center;
        height: 150px;
    }
</style>

<style>
      .pagination li.page-item.active a {
            border-radius: 5px;
            background-color: #bd7f02 !important;
            border: 1px solid var(--colorBorder);
            color: #fff !important;
            font-size: 0.9375rem;
            border: 1px solid var(--colorSecondary);
            border: 1px solid var(--colorBorder);
            color: #fff !important;
            font-size: 0.9375rem;
            border: 1px solid var(--colorBorder);
            color: #fff !important;
            font-size: 0.9375rem;
            border: 1px solid var(--colorSecondary);          
      }
      .pagination li.page-item a {
        border-radius: 5px;
        border: 1px solid #dff1ff;
        background-color: #600033 !important;
        color: #fff !important;
        font-size: 0.9375rem;
        border: 1px solid var(--colorSecondary);
        font-size: 0.9375rem;
        padding: 0.375rem 0.75rem;
        position: relative;
        display: block;
        text-decoration: none;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }

    .pagination li.page-item a:hover, .pagination li.page-item.active a:hover {
      background: var(--colorPrimary);
      border: 1px solid var(--colorPrimary);
      color: #fff;
    }

</style>

<script>
    $(document).ready(function() {
        filter_data(page = 1);
        var minimum_price = '';
        var maximum_price = '';
        $('#get_price').on('click', function(){
          minimum_price = $('.rangeValues--min').attr('data-val');
          maximum_price = $('.rangeValues--max').attr('data-val');
          if(minimum_price == 0) {
            minimum_price = 1;
          }
          filter_data(page = 1,  minimum_price, maximum_price);
        });

        function filter_data(page, min, max) {
            $('.filter_data').html('<div id="loading" style="" ><img src="<?php echo base_url('assets/images/loader.gif'); ?>"></div>');
            var action = 'fetch_data';

            var brand = get_filter('brand');
            var ram = get_filter('ram');
            var storage = get_filter('storage');
            $.ajax({
                url: "<?php echo base_url(); ?>product/fetch_data/" + page,
                method: "POST",
                dataType: "JSON",
                data: {
                    action: action,
                    minimum_price: min,
                    maximum_price: max,
                    brand: brand,
                    ram: ram,
                    storage: storage
                },
                success: function(data) {
                    $('.filter_data').html(data.product_list);
                    $('#pagination_link').html(data.pagination_link);
                }
            })
        }

        function get_filter(class_name) {
            var filter = [];
            $('.' + class_name + ':checked').each(function() {
                filter.push($(this).val());
            });
            return filter;
        }

        $(document).on('click', '.pagination li a', function(event) {
            event.preventDefault();
            var page = $(this).data('ci-pagination-page');
            filter_data(page);
        });

        $('.common_selector').click(function() {
            filter_data(1);
        });

        // $('#price_range_2').slider({
        //   range:true,
        //   min:1000,
        //   max:65000,
        //   values:[1000,65000],
        //   step:500,
        //   stop:function(event, ui)
        //   {
        //       $('#price_show').html(ui.values[0] + ' - ' + ui.values[1]);
        //       $('#hidden_minimum_price').val(ui.values[0]);
        //       $('#hidden_maximum_price').val(ui.values[1]);
        //       filter_data(1);
        //   }
        // });
    });
</script>

<?php $this->load->view('template/footer');?>
