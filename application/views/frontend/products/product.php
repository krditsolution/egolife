<?php $this->load->view('template/header'); ?>

<main class="site-content site-content--softwares">
    <section class="pagetitle-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">Softwares</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>Softwares</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="section--py softwares-wrapper-section">
        <div class="container">
            <div class="section__header text-center">
                <h2 class="text-center text-dark fw-bold">Download Softwares</h2>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-10 col-md-9 col-lg-8 col-xl-7 col-xxl-6">
                        <p class="text-para">You can download software by clicking the download link given bellow</p>
                        <form action="#" class="form software-search-form">
                            <div class="form__field"><input name="ssf_inp" class="form__input" placeholder="Search keyword here..."><span class="icon"><i class="bi bi-search"></i></span></div>
                        </form>
                    </div>
                </div>
            </div>
            <?php if (!empty($data['softwars'])) : ?>
                <div class="soft-items-grid">
                    <?php foreach ($data['softwars'] as $softwars) : ?>
                        <?php
                        $image = json_decode($softwars['img'], true);
                        ?>
                        <div class="soft-item">
                            <div class="soft-item__header"><img src="<?php echo base_url('assets/images/software/' . $image['images_0']) ?>" class="w-100 h-100 fit-cover" width="400" height="300" alt="Software Title"></div>
                            <div class="soft-item__body">
                                <div class="soft-item__title"><a style="font-size: 20px;" href="<?php echo $softwars['url'] ?>"><?php echo $softwars['name'] ?></a></div>
                                <div class="soft-item__cta-wrapper"><a href="<?php echo $softwars['url'] ?>" class="btn btn-primary soft-item__btn--download d-block w-100 text-center">Download<span class="ms-1"><i class="fa-solid fa-download"></i></span></a></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php else : ?>
                <div class="img__holder">
                    <img src="<?php echo base_url() ?>assets/images/no_data.webp" alt="" style="margin: 0 auto; height:220px;">
                    <h6 class="mb-0 text-danger soft-count--empty text-center">Sorry, No Software found.</h6>
                </div>
            <?php endif; ?>
        </div>
    </section>
</main>

<?php $this->load->view('template/footer'); ?>