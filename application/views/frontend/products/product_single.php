<?php $this->load->view('template/header');

$images = json_decode($data['product_details'][0]['product_img'], TRUE);
$color = json_decode($data['product_details'][0]['color'], TRUE);
$size = json_decode($data['product_details'][0]['size'], TRUE);
// echo "<pre>";
// print_r($data);
//echo "<pre>"; echo $this->db->last_query(); echo "<br><br><br><br>"; print_r($data);exit();
?>

<main class="site-content site-content--product-single">
    <section class="pagetitle-section d-flex align-items-center justify-content-center">
        <div class="container">
            <div class="pagetitle-block py-4 border-top border-bottom">
                <h3 class="text-primary py-2 text-uppercase text-center" style="font-size: 1.5rem;"><?php echo $data['product_details'][0]['product_name']?></h3>
            </div>
        </div>
    </section>

    <section class="product-single-section section--py">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-lg-10 col-xl-10 mb-4">
                    <div class="row">
                        <div class="col-12 col-md-6 pb-3 pb-md-0 pe-xl-3 position-relative">
                            

                            <div class="prouduct-single__thumbnail-image-gallery row flex-column-reverse flex-xl-row">
                                <!-- Product Single Thumbnail View -->
                                <div class="col-12 col-md-12 col-xl-2 mt-2 mt-xl-0">
                                    <div thumbsslider="" class="h-100 swiper product-single-swiper-thumb d-none d-lg-block">
                                        <div class="swiper-wrapper">
                                            <?php for($i = 0; $i <count($images); $i++):?>
                                                <div class="swiper-slide">
                                                    <img src="<?php echo base_url('/assets/images/products/').$images['images_'.$i]?>" class="w-100 h-100 fit-cover">
                                                </div>
                                            <?php endfor;?>
                                        </div>
                                        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                                    </div>
                                </div>
                                <!-- Product Single - Large View -->
                                <div class="col-12 col-md-12 col-xl-10 position-relative">
                                    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper product-single-swiper-view">
                                        <div class="swiper-wrapper">
                                            <?php for($i = 0; $i <count($images); $i++):?>
                                                <div class="swiper-slide">
                                                    <img class="img-responsive" src="<?php echo base_url('/assets/images/products/').$images['images_'.$i]?>" class="w-100 h-100 fit-cover">
                                                </div>
                                            <?php endfor;?>
                                        </div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                        <span class="swiper-notification"></span>
                                    </div>
                                </div>
                                <!-- Product Gallery Magnific Popup -->
                                <div class="product-single__magnific-container d-none">
                                    <?php for($i = 0; $i <count($images); $i++):?>
                                        <a href="https://dummyimage.com/400x600/000/fff"></a>
                                    <?php endfor;?>
                                </div>
                            </div>


                        </div>

                        <div class="col-12 col-md-6 pt-4 pt-md-0 ps-md-4 ps-xl-4">
                            <h2 class="product-single__title text-dark"><?php echo $data['product_details'][0]['product_name']?></h2>
                            <p class="mt-0"><?php echo $data['product_details'][0]['product_title']?></p>
                            <div class="row mb-4 align-items-center">
                                <div class="col-12 col-lg-11 col-xl-9 col-xxl-8 product-single__price-wrapper">
                                    <!-- Product Sngle Pricing -->
                                    <div class="product-single__price-container d-flex align-items-end">
                                        <h3 class="product-single__sale-price text-primary mb-0"> &#8377;<?php echo $data['product_details'][0]['product_selling_price']?></h3>
                                        <h5 class="product-single__regular-price text-para mb-0">&#8377;<?php echo $data['product_details'][0]['product_mrp']?></h5>
                                        <h6 class="product-single__discount-value mb-0 ms-3"><?php echo (int)((($data['product_details'][0]['mrp'] - $data['product_details'][0]['selling_price'])/$data['product_details'][0]['mrp'])*100) ;?>% Off</h6>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="product-single__review-container d-none">
                                        <span class="product-single__review-stars me-2">
                                            <i class="bi bi-star-fill"></i>
                                            <i class="bi bi-star-fill"></i>
                                            <i class="bi bi-star-fill"></i>
                                            <i class="bi bi-star-fill"></i>
                                            <i class="bi bi-star-fill"></i>
                                        </span>
                                        <span class="product-single__review-value me-2 me-lg-0">5.00</span>
                                        <span class="product-single__review-submission d-lg-block"></span>
                                    </div>
                                </div>
                            </div>
                            <!-- Product Single Key Highlights -->
                            <div class="mb-4 pt-2">
                                <h6 class="text--heading text-uppercase fw-bold font--heading">Highlights</h6>
                                <ul class="key-highlights pb-2">
                                    <li class="key-highlight-item">
                                        <div class="key-highlight-item__prefix">-</div>
                                        <div class="key-highlight-item__text"><?php echo $data['product_details'][0]['features']?></div>
                                    </li>
                                   
                                </ul>
                            </div>

                            <!-- Product Single Check Pincode availability -->
                            <!-- <div class="pc-avail-block py-3 border-top border-bottom mb-4">
                                <div class="row py-1">
                                    <div class="col-12 col-sm-6">
                                        <p class="text-xs text-dark text-uppercase mb-0">Check Delivery Estimate For Your Pincode</p>
                                    </div>
                                    <div class="col-12 col-sm-6">
                                        <form action="#" class="form pc-avail-form w-100">
                                            <div class="form__field m-0 bg-white border border-1 rounded-pill d-flex">
                                                <input type="number" name="inpPinCode" placeholder="Pincode" class="border-0 form__input rounded-pill py-2 px-3" required>
                                                <button type="submit" class="btn btn-secondary px-3 py-2">Check</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div> -->

                            <div class="product-single__color-wrapper d-flex flex-wrap align-items-center mb-4 pt-2">
                                <p class="text-dark mb-0 text-uppercase text-sm d-inline-block me-2">Color:</p>
                                <?php foreach($color as $colors):?>
                                    <?php 
                                        $CI =& get_instance();
                                        $color_data = $CI->Common_model->get_data_from_table_by_id('color', $colors);
                                    ?>
                                    <a href="javascript:void(0)" class="product_color active" data-color="<?php echo $color_data[0]['color_name'];?>"><span style="background-color: <?php echo $color_data[0]['color_code']?>;"></span></a>
                                <?php endforeach;?>
                            </div>

                            <div class="d-flex flex-wrap align-items-center mb-4 pt-2">
                                <p class="text-dark mb-0 text-uppercase text-sm d-inline-block me-2">Size:</p>
                                <?php foreach($size as $sizes):?>
                                    <?php //echo "<pre>"; print_r($sizes);?>
                                    <a style="border-color: #fff !important;" href="javascript:void(0)" class="product_size me-2 d-inline-block" data-size="<?php echo $sizes;?>"><span><?php echo $sizes;?></span></a>
                                <?php endforeach;?>
                            </div>
                            <!-- Product CTA Buttons wrapper -->
                            <div class="product-single__cta-wrapper mb-4 pt-2">
                                <div class="d-flex align-items-center mb-4">
                                    <div class="product-single__quantity-button-wrapper d-flex align-items-center me-3">
                                        <div class="form__field d-flex flex-xl-wrap align-items-center mb-3 mb-xl-0">
                                            <div class="product-single__quantity-button quantity-generating-input-container">
                                                <input type="button" value="-" class="qtyminus" field="quantity" />
                                                <input type="text" name="quantity" value="1" class="qty" />
                                                <input type="button" value="+" class="qtyplus" field="quantity" />
                                                <input type="hidden" value="<?php echo $data['product_details'][0]['product_id']?>" class="product_id" field="quantity" />
                                            </div>
                                        </div>
                                        <?php if($this->session->userdata('user_id')>0):?>
                                            <a href="javascript:void(0)" class="product-single__cta--wishlist ms-3 product-card__cta-button--wishlist" data-color="" data-size="<?php //echo $size;?>" data_product_id="<?php echo $data['product_details'][0]['product_id']?>" data_userid="<?php echo $this->session->userdata('user_id');?>">
                                                <i class="bi bi-heart-fill"></i>
                                                <span class="tooltip-text">Add To Wishlist</span>
                                            </a>
                                            <!-- <a href="javascript:void(0)" class="btn btn-primary d-inline-block product-single__cta--add-to-cart product-card__cta-button--cart">Buy Now</a> -->
                                        <?php endif;?>
                                    </div>
                               </div>
                               <div class="d-flex align-items-center mb-4">
                                    <a href="javascript:void(0)" class="btn btn-primary d-inline-block product-single__cta--add-to-cart product-card__cta-button--cart">Add to Cart</a>
                                    <?php //if($this->session->userdata('user_id')):?>
                                   <!-- <a href="javascript:void(0)" class="btn btn-secondary ms-3 d-inline-block product-single__cta--add-to-cart " style="<?php //if(!$this->session->userdata('user_id')>0){echo 'pointer-events: none;opacity: .5;';}?>">Buy Now</a>-->
                                    <?php //endif;?>
                               </div>
                               
                            </div>

                            <!-- Product Single Meta Info -->
                            <div class="product-single__meta-info pt-2">
                                <div class="row flex--xl-nowrap align-items-center">
                                    <div class="col-12 col-md-12 col-xl-7 site-facility-blocks-wrapper mb-3 mb-xl-0">
                                        <div class="row">
                                            <div class="col-4">
                                                <div class="site-facility-block">
                                                    <div class="site-facility-block__icon text-secondary">
                                                        <i class="fa-solid fa-truck"></i>
                                                    </div>
                                                    <p class="site-facility-block__text text-dark mb-0 fw--semibold text-xs">Free<br>Shipping</p>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="site-facility-block">
                                                    <div class="site-facility-block__icon text-secondary">
                                                        <i class="fa-solid fa-money-bill-wave"></i>
                                                    </div>
                                                    <p class="site-facility-block__text text--dark mb-0 fw--semibold text-xs">Cash On<br>Delivery</p>
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="site-facility-block">
                                                    <div class="site-facility-block__icon text-secondary">
                                                        <i class="fa-solid fa-rotate-left"></i>
                                                    </div>
                                                    <p class="site-facility-block__text text-dark mb-0 fw--semibold text-xs">Easy<br>Returns</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-12 col-xl-5 ps-xl-4">
                                        <p class="text--para mb-1 text--sm">Share this product:</p>
                                        <ul class="product-single__social-sharing d-flex align-items-center">
                                            <li class="social-sharing__item">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url('product-details/'.$data['product_details'][0]['id'])?>" class="social-sharing__item--link fb--link" target="_blank">
                                                    <span class="social-sharing__item-icon"><i class="bi bi-facebook"></i></span>
                                                    <span class="social-sharing__item-tooltip tooltip-text">Facebook</span>
                                                </a>
                                            </li>
                                            <li class="social-sharing__item">
                                                <a href="https://twitter.com/share?url=<?php echo $data['product_details'][0]['name']."  ";echo "  ".base_url('product-details/'.$data['product_details'][0]['id'])?> &hashtags=Daversi" class="social-sharing__item--link twitter--link" target="_blank">
                                                    <span class="social-sharing__item-icon"><i class="fa-brands fa-twitter"></i></span>
                                                    <span class="social-sharing__item-tooltip tooltip-text">Twitter</span>
                                                </a>
                                            </li>
                                            <li class="social-sharing__item">
                                                <a href="https://api.whatsapp.com/send?text=<?php echo base_url('product-details/'.$data['product_details'][0]['id']);?> *Please Check out The Product*" class="social-sharing__item--link whatsapp--link" target="_blank">
                                                    <span class="social-sharing__item-icon"><i class="fa-brands fa-whatsapp"></i></span>
                                                    <span class="social-sharing__item-tooltip tooltip-text">Whatsapp</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-5 product-single-info-tab pt-3">
                    <nav class="product-single__tab d-none d-md-flex">
                        <div class="nav nav-tabs position-relative w-100" id="nav-tab" role="tablist">
                            <button class="nav-link active" id="nav-description-tab" data-bs-toggle="tab" data-bs-target="#nav-description" type="button" role="tab" aria-controls="nav-description" aria-selected="true">Description</button>
                            <button class="nav-link" id="nav-addi-info-tab" data-bs-toggle="tab" data-bs-target="#nav-addi-info" type="button" role="tab" aria-controls="nav-addi-info" aria-selected="false">Additional information</button>
                            <button class="nav-link" id="nav-reviews-tab" data-bs-toggle="tab" data-bs-target="#nav-reviews" type="button" role="tab" aria-controls="nav-reviews" aria-selected="false">Reviews</button>
                        </div>
                    </nav>
                    <div class="tab-content d-none d-md-block" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-description" role="tabpanel" aria-labelledby="nav-description-tab">
                            <div class="product-single__description">
                                <p class="text--para">
                                    <?php echo $data['product_details'][0]['product_description']?>
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="nav-addi-info" role="tabpanel" aria-labelledby="nav-addi-info-tab">
                            <div class="product-single__addi-info mb-5">
                                <?php echo $data['product_details'][0]['features']?>
                            </div>
                        </div>
                        <div class="tab-pane fade d-none" id="nav-reviews" role="tabpanel" aria-labelledby="nav-reviews-tab">
                            <div class="product-single__review-wrapper">
                                <div class="review-block d-md-flex flex-sm-nowrap bg--light p-3 border-radius">
                                    <div class="review-block__profile-img-container overflow-hidden rounded-circle">
                                        <img src="https://dummyimage.com/600x400/000/fff" alt="Profile Title" class="w-100 h-100">
                                    </div>
                                    <div class="review-block__content ps-md-3 ps-lg-4">
                                        <div class="review-block__header d-flex pb-1 pb-md-2">
                                            <p class="review-block__profile mb-0 me-md-2 fw-bold">John Doe</p>
                                            <p class="review-block__date mb-0">June 20, 2022</p>
                                        </div>
                                        <div class="review-block__body pt-1 pt-md-2">
                                            <p class="review-block__comment fw--semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. In iusto doloribus suscipit odit? Assumenda facere necessitatibus quam quae. Vitae, quam.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="review-block d-md-flex flex-sm-nowrap bg--light p-3 border-radius">
                                    <div class="review-block__profile-img-container overflow-hidden rounded-circle">
                                        <img src="https://dummyimage.com/600x400/000/fff" alt="Profile Title" class="w-100 h-100">
                                    </div>
                                    <div class="review-block__content ps-md-3 ps-lg-4">
                                        <div class="review-block__header d-flex pb-1 pb-md-2">
                                            <p class="review-block__profile mb-0 me-md-2 fw-bold">John Doe</p>
                                            <p class="review-block__date mb-0">June 20, 2022</p>
                                        </div>
                                        <div class="review-block__body pt-1 pt-md-2">
                                            <p class="review-block__comment fw--semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. In iusto doloribus suscipit odit? Assumenda facere necessitatibus quam quae. Vitae, quam.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-container d-md-none">
                        <div class="set">
                            <a href="javascript:void(0)">
                                Description
                                <i class="fa fa-plus"></i>
                            </a>
                            <div class="content">
                                <p><?php echo $data['product_details'][0]['product_description']?></p>
                            </div>
                        </div>
                        <div class="set">
                            <a href="javascript:void(0)">
                                Additional Information
                                <i class="fa fa-plus"></i>
                            </a>
                            <div class="content">
                                <p><?php echo $data['product_details'][0]['features']?></p>
                            </div>
                        </div>
                        <div class="set">
                            <a href="javascript:void(0)">
                                Reviews(2)
                                <i class="fa fa-plus"></i>
                            </a>
                            <div class="content">
                                <div class="product-single__review-wrapper">
                                    <div class="review-block d-md-flex flex-sm-nowrap bg--light p-3 border-radius">
                                        <div class="review-block__profile-img-container overflow-hidden rounded-circle">
                                            <img src="https://dummyimage.com/600x400/000/fff" alt="Profile Title" class="w-100 h-100">
                                        </div>
                                        <div class="review-block__content ps-md-3 ps-lg-4">
                                            <div class="review-block__header d-flex pb-1 pb-md-2">
                                                <p class="review-block__profile mb-0 me-md-2 fw-bold">John Doe</p>
                                                <p class="review-block__date mb-0">June 20, 2022</p>
                                            </div>
                                            <div class="review-block__body pt-1 pt-md-2">
                                                <p class="review-block__comment fw--semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. In iusto doloribus suscipit odit? Assumenda facere necessitatibus quam quae. Vitae, quam.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="review-block d-md-flex flex-sm-nowrap bg--light p-3 border-radius">
                                        <div class="review-block__profile-img-container overflow-hidden rounded-circle">
                                            <img src="https://dummyimage.com/600x400/000/fff" alt="Profile Title" class="w-100 h-100">
                                        </div>
                                        <div class="review-block__content ps-md-3 ps-lg-4">
                                            <div class="review-block__header d-flex pb-1 pb-md-2">
                                                <p class="review-block__profile mb-0 me-md-2 fw-bold">John Doe</p>
                                                <p class="review-block__date mb-0">June 20, 2022</p>
                                            </div>
                                            <div class="review-block__body pt-1 pt-md-2">
                                                <p class="review-block__comment fw--semibold">Lorem ipsum dolor sit amet consectetur adipisicing elit. In iusto doloribus suscipit odit? Assumenda facere necessitatibus quam quae. Vitae, quam.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>

    <div class="divider-block">
        <div class="container">
            <div class="custom-divider"></div>
        </div>
    </div>

    <!-- You May Like -->
    <section class="you-may-like-section pt-4 section--pb">
        <div class="container pt-2">
            <h4 class="text-dark text-uppercase fw-normal text-center mb-5">You May Also Like</h4>
            <div class="products-grid-container">
                <?php foreach ($data['products'] as $key => $products):?>
                    <?php $images = json_decode($products['img'], TRUE);?>
                <div class="product-item-main">
                    <div class="product-item-main__header">
                        <a href="<?php echo base_url('product-details/'.$products['id']);?>" class="product-item-main__thumbnail-container">
                            <img src="<?php echo base_url('assets/images/products/'.$images['images_0']);?>" alt="Dummy Product Image">
                        </a>
                    </div>
                    <div class="product-item-main__body">
                        <a class="product-item-main__title" href="<?php echo base_url('product-details/'.$products['id']);?>"><?php echo $products['name'];?></a>
                        <div class="product-item-main__price-container d-flex align-items-center product-item-main fw-semibold ">
                            <span class="sale-price text-primary">&#8377 <?php echo $products['selling_price']?></span>
                            <span class="regular-price text-dark">&#8377 <?php echo $products['mrp']?></span>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
</main>

<?php $this->load->view('template/footer');?>