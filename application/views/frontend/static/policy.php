<?php $this->load->view('template/header');?>

<main class="site-content site-content--about">
    <section class="pagetitle-section">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">Privacy Policy</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>Privacy Policy</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="section--py">
        <div class="container">
            <p class="text-para">WELCOME 
        This site is owned by Egolife Capital. Our deepest concern is for your privacy. We want to protect the personal information you give us because we appreciate the responsibility that comes with handling the visitors' personal information. Our procedures for information collection, use, and disclosure are outlined in this privacy policy. PRIVACY POLICY SHOULD BE READ CAREFULLY BEFORE USING THIS WEBSITE OR SUBMITTING ANY INFORMATION. ACCORDING TO OUR PRIVACY POLICY, BY USING THIS WEBSITE, YOU AGREE.
            </p>

            <p class="text-para">
                Only information that you voluntarily supplied, is accepted by us. We provide you the freedom to use our website without giving any personal information because we understand the privacy issues associated with doing so. You may also choose, at your discretion, to reveal private or restricted information. We pledge to treat the information you offer us with the highest confidentiality since we value your trust as a guest. The promise of no disclosure to any third party comes with the stringent protection of the contact information, which includes the name, email address, and phone number.
            </p>
            <h5 class="text-dark">
                HOW EGOLIFE CAPITAL COLLECTS YOUR INFORMATION? 
            </h5>
            <p class="text-para">
            EGOLIFE CAPITAL collects personally identifiable information about our visitors either directly through forms that they fill up on our site, or by placing cookies in the user’s hard disk as they browse this website. The website may place cookies on your computer’s hard disk when you visit our website, Post comments on the blog, Subscribe to newsletters.<br>
            THE SERVER MAY ACCESS COOKIES IN YOUR COMPUTER’S HARD DISK WHEN YOU VISIT THIS WEBSITE AGAIN, FOR: Recognizing your computer. Displaying pages according to your preferred display settings. This includes, without limitation, page layout and font sizes. Helping you fill out forms quicker, by completing some of the entries automatically as per the information provided by you in the past. Identifying a returning user. Also, to track the number of unique visitors accessing this website, on a daily basis. E-MAIL ADDRESS, PHONE NO: The email address and phone number provided in the ‘contact us’ form will be used to share vital information with you. This information can be anything related to your query, projects, payments. Once entered into our mailing list, you will be regularly sent newsletters and other promotional notifications of our company or our related partners. Giving optimum value to your consent to receive all this, we give you the liberty to get unauthorized from our communications by deleting your details from our database. With our associates obliged by the conditions of our privacy policy, you get the option of disconnecting yourself from regular communications from their side as well. CREDIT/DEBIT CARD DETAILS: The credit and debit card details given by you regarding your payment are purely used only for processing purposes. We do not store this data at our end and take all steps to secure your data while it is in our possession. We reserve the right of using this data like the billing address for future client concerns and render the assurance of not sharing this information with a third party. DISCLOSURE OF INFORMATION UNDER SPECIAL PROVISIONS: Governing the client data under a set of rules and regulations, you should understand that EGOLIFE CAPITAL will be applicable for revealing certain sections of the customer data under the following circumstances: Post a written approval from your side Post an order from any sort of legal jurisdiction In the context of societal benefit In compliance with the protection rights of EGOLIFE CAPITAL, THE INFORMATION GIVEN ON THE WEBSITE CAN BE SHARED WITH THE FOLLOWING GROUPS OF INDIVIDUALS: 
                To a related associate of EGOLIFE CAPITAL To an individual or individuals to whom we are obliged to do so as per law.<br>
                To personnel like the lawyers and financial consultants for the proper facilitation of required proceedings. Holding the right to use the information stated by you for promotional purposes, we however guarantee removing your details from our database post to your request of doing so. Responding to your requirements promptly, we take all the necessary efforts to keep you satisfied with our work policy. WE AT EGOLIFE CAPITAL, TAKE YOUR PRIVACY VERY SERIOUSLY. WE WILL NEVER SELL ANY OF YOUR INFORMATION TO ANY THIRD-PARTY AUTHORITIES. The information that you have provided to us is securely stored on our servers. It is only used for providing better services to you. CHANGE OF POLICY: Please be noted that the above-mentioned privacy policy is subject to change and will be published on our website with immediate effect. It is in these regards that you are recommended to go through the clauses briefed over here for an apt understanding of our working procedure. AGREEMENT TO THE POLICY: You should be aware that by submitting any kind of personal details to our website, you indicate your acceptance of the terms given above. In case of any queries or concerns, you are always free to contact us for further assistance.


            </p>
        </div>
    </section>
</main>  

<?php $this->load->view('template/footer');?>

