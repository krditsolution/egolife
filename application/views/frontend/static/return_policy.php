<?php $this->load->view('template/header');?>

<main class="site-content site-content--about">
    <!-- page title -->
    <section class="pagetitle-section d-flex align-items-center justify-content-center mb-5">
        <div class="pagetitle-block py-4 border-top border-bottom">
            <h3 class="text-primary py-2 text-uppercase">Return Policy</h3>
        </div>
    </section>
    <section class='container site-content mt-5'>
        <span id="docs-internal-guid-73f26045-7fff-1650-362e-bedec4083c3b"><p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 12pt; font-family: Poppins, sans-serif; color: rgb(74, 74, 74); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">We may contact you to ascertain the damage or defect in the product prior to issuing refund/replacement. The product will be refunded by the courier on account that the courier cost needs to be paid by the customer.&nbsp;</span></p><p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p><p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 12pt; font-family: Poppins, sans-serif; color: rgb(74, 74, 74); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">Refunds will be through bank transfers. No cash refunds will be made. It would take our team at least 5 to 7 business days post refund initiation for online refund to be initiated.</span></p><p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;">&nbsp;</p><p dir="ltr" style="line-height: 1.38; margin-top: 0pt; margin-bottom: 0pt;"><span style="font-size: 12pt; font-family: Poppins, sans-serif; color: rgb(74, 74, 74); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;">In case of NEFT, it would require us at least 3 to 5 business days post refund initiation to refund your amount. For Prepaid orders the amount shall be credited to your account through which the payment was done. It may take 7-10 business days for the amount to reflect in your account.&nbsp;</span></p><div><span style="font-size: 12pt; font-family: Poppins, sans-serif; color: rgb(74, 74, 74); background-color: transparent; font-variant-numeric: normal; font-variant-east-asian: normal; vertical-align: baseline; white-space: pre-wrap;"><br></span></div></span>
    </section>
</main>    

<?php $this->load->view('template/footer');?>