<?php 
    // echo "<pre>";
    // print_r($data);
    $CI =& get_instance();
    $user_details = $CI->Common_model->get_data_from_table_by_id('users', $data['user_id']);

    // echo "--------------------<br>";
    // print_r($user_details[0]['name']);
    // // exit();
?>
<button id="rzp-button1"></button>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options = {
    "key": "<?php echo $data['key_id']?>", // Enter the Key ID generated from the Dashboard
    "amount": "<?php echo $data['order_data']['amount']?>", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "Viva Vaazar",
    "description": "Payment For Your Order",
    "image": "<?php echo base_url()?>assets/images/viva-logo.png",
    "order_id": "<?php echo $data['order_data']['id']?>", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "callback_url": "<?php echo base_url('payment/success')?>",
    "prefill": {
        "name": "<?php echo $user_details[0]['name']?>",
        "email": "<?php echo $user_details[0]['email_addr']?>",
        "contact": "<?php echo $user_details[0]['mobile_number']?>"
    },
    "notes": {
        "address": "Razorpay Corporate Office"
    },
    "theme": {
        "color": "#3399cc"
    }
};
var rzp1 = new Razorpay(options);
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
document.getElementById('rzp-button1').click();
</script>
