<?php $this->load->view('template/header');?>


<div style="clear:both"></div>

<section class="container mt-5 mb-5">
  <div class="tab">
    <button class="tablinks" onclick="openCity(event, 'user_details')" id="defaultOpen">Personal Details</button>
    <button class="tablinks" onclick="openCity(event, 'address')">Address</button>
    <button class="tablinks" onclick="openCity(event, 'Tokyo')">My Orders</button>
    <!-- <button class="tablinks" onclick="openCity(event, 'wishlist')">Wishlist</button>
    <button class="tablinks" onclick="openCity(event, 'cart')">Cart</button> -->
  </div>

  <div id="user_details" class="tabcontent mb-5">
    <h4 class="text-center mb-3 mt-3">Personal Details</h4>
    <div class="card mb-3">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Full Name</h6>
          </div>
          <div class="col-sm-9 text-secondary">
            <?php echo $data['user_details'][0]['name'];?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Email</h6>
          </div>
          <div class="col-sm-9 text-secondary">
          <?php echo $data['user_details'][0]['email_addr']?>
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="col-sm-3">
            <h6 class="mb-0">Phone</h6>
          </div>
          <div class="col-sm-9 text-secondary">
          <?php echo $data['user_details'][0]['mobile_number']?>
          </div>
        </div>
      
        <hr>
        <div class="row">
          <div class="col-sm-12">
            <a class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal-1">Edit</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="address" class="tabcontent">
    <div class="row">
      <div class="col-md-6 col-xs-6 text-center"><h4 class=" mb-3 mt-3">Address</h4></div>
      <!-- <div class="col-md-6 col-xs-6 text-center"> <a class="btn btn-primary text-right mb-3 mt-3" href="" data-toggle="modal" data-target="#addAddress"> Add</a></div> -->
    </div>
    
    <div class="row">
      <?php foreach($data['get_user_address_by_id'] as $key=>$get_user_address_by_id):?>
        <div class="col-md-6 mb-3">
          <div class="card" <?php if($get_user_address_by_id['is_default']>0){echo 'style = "border: 1px solid rgb(22 138 145)" ';}?>>
              <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                      <div class="mt-3">
                          <p class="text-muted font-size-sm">
                              <?php echo $get_user_address_by_id['address'].", <br>".$get_user_address_by_id['city'].', '.$get_user_address_by_id['state'].", ".$get_user_address_by_id['zip_code'].',<br> Near:'.$get_user_address_by_id['landmark']?>
                          </p>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      <?php endforeach;?>
    </div>
  </div>

  <div id="Tokyo" class="tabcontent">
    <h4 class="text-center mb-3 mt-3">My Orders</h4>
    <div class="row">

    <?php if(!empty($data['get_orders_by_user_id'])):?>
      <table id="dataTable" class="table table-striped table-responsive-xs table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><b>#</b></th>
                <th>Order Id </th>
                <th>Final Price</th>
                <th>Order Status</th>
                <th>Payment Status</th>
                <th>Order date</th>
                <th>Order info</th>
            </tr>
        </thead>
        <tbody>
          <?php $i = 1; foreach($data['get_orders_by_user_id'] AS $key=>$get_orders_by_user_id):?>
            <tr>
                <td><b><?php echo $i;?></b></td>
                <td><?php echo 'VIVA00'.$get_orders_by_user_id['id'];?></td>
                <td>&#8377;<?php echo '<b>'.$get_orders_by_user_id['total_price'].'</b>';?></td>
                <td> <p class="bg-warning text-white d-inline-block p-1 rounded-1 text-center"><?php echo $get_orders_by_user_id['order_status'];?></p></td>
                <td><p class="bg-success text-white d-inline-block p-1 rounded-1 text-center"><?php echo $get_orders_by_user_id['payment_status'];?></p></td>
                <td><?php echo $get_orders_by_user_id['date_time'];?></td>
                <td><a href="<?php //echo base_url('order-details/'.$get_orders_by_user_id['id'])?>">Order info</a> </td>
            </tr>
          <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
              <th><b>#</b></th>
              <th>Order Id </th>
              <th>Final Price</th>
              <th>Order Status</th>
              <th>Payment Status</th>
              <th>Order date</th>
              <th>Order info</th>
            </tr>
        </tfoot>
        <tbody>
            
                <?php 
                //$sr = 1;
                    //foreach($data[0]['user_orders'] as $orders){ ?>
                        <tr>
                            <td><?php //echo $sr ;?></td>
                            <td><?php //echo 'AWAK00'.$orders['id'] ;?></td>
                            <td class="text-success"> </td>
                            <td>  </td>
                            <td><?php //echo $orders['date_time'];?></td>
                            <td>
                              <!-- <a href="<?php //echo base_url('index.php/home/order_details/').$orders['id'];?>" style="padding:10px"><i class="bi bi-eye h4"></i> </a> -->
                              <!-- <a style="padding:10px"><i class="fa fa-eye"></i> </a>
                              <a style="padding:10px"><i class="fa fa-eye"></i> </a> -->
                            </td>
                        </tr>
                    <?php //$sr++;  }
                  ?>
        </tbody>
      </table>
    <?php else:?>
      <p class="empty-cart-text text-center error-text mt-3 mb-5 h4">You Don't Have any Orders</p>
    <?php endif;?>
    </div>
  </div>
  
  <div id="wishlist" class="tabcontent">
    <div class="row">
        <h4 class=" mb-3 mt-3">Wishlist</h4>
        <?php foreach ($data['all_wishlist_item'] as $key => $value):?>
          <div class="col-md-6 col-xs-6 text-center">
            <table class="table wishlist-table">
              <tbody>
                <tr class="wishlist-table__row">
                  <td>
                    <div class="text-center">
                      <a href="<?php echo base_url('user/delete_wishlist/').$value['product_id']?>" class="wishlist-product__button--remove"><i class="bi bi-x-lg"></i></a>
                    </div>
                  </td>
                  <td>
                    <div>
                      <a href="<?php echo base_url('product-details/').$value['product_id']?>" class="wishlist-product--link">
                        <img src="<?php echo base_url('assets/images/products/').json_decode($value['img'],TRUE)['images_0']?>" alt="product" width="48">
                      </a>
                    </div>
                  </td>
                  <td>
                    <div class="wishlist-product-container text-center">
                      <p class="wishlist-product__title text--heading mb-2">
                        <a href="<?php echo base_url('product-details/').$value['product_id']?>"><?php echo $value['name']?></a>
                      </p>
                      <p class="wishlist-product__price-container text-center mb-2">
                        <span class="price-sale text--lg font-weight-bold text--secondary">&#8377;<?php echo $value['selling_price']?></span>
                        <span class="price-regular ms-1 text--para mb-0">&#8377;<?php echo $value['mrp']?></span>
                      </p>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        <?php endforeach?>
    </div>
  </div>
  <div id="cart" class="tabcontent">
    <div class="row">
        <h4 class=" mb-3 mt-3">Cart</h4>
        <?php foreach ($data['all_cart_item'] as $key => $value):?>
          <div class="col-md-6 col-xs-6 text-center">
            <table class="table wishlist-table">
              <tbody>
                <tr class="wishlist-table__row">
                  <td>
                    <div class="text-center">
                      <a onclick="deletecartproduct(<?php echo $value['product_id']?>)" class="wishlist-product__button--remove"><i class="bi bi-x-lg"></i></a>
                    </div>
                  </td>
                  <td>
                    <div>
                      <a href="<?php echo base_url('home/product_details/').$value['product_id']?>" class="wishlist-product--link">
                        <img src="<?php echo $value['img']?>" alt="product" width="48">
                      </a>
                    </div>
                  </td>
                  <td>
                    <div class="wishlist-product-container text-center">
                      <p class="wishlist-product__title text--heading mb-2">
                        <a href="<?php echo base_url('home/product_details/').$value['product_id']?>"><?php echo $value['name']?></a>
                      </p>
                      <p class="wishlist-product__price-container text-center mb-2">
                        <span class="price-sale text--lg font-weight-bold text--secondary">$<?php echo $value['selling_price']?></span>
                        <span class="price-regular ms-1 text--para mb-0">$<?php echo $value['mrp']?></span>
                      </p>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        <?php endforeach?>
    </div>
  </div>

  <script>
      function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
      }
      // Get the element with id="defaultOpen" and click on it
      document.getElementById("defaultOpen").click();
  </script>
</section>
<div class="clear" style="clear:both"></div>

<!-- Modal for Edite Personal Details -->
<div class="modal fade" id="exampleModal-1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Personal Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
              <?php echo form_open_multipart('index.php/home/edit_Personal_details');?>
                <div class="form-group">
                    <label for="exampleInputPassword1">Name</label>
                    <?php echo form_input(['name' => 'name', 'class' => 'form-control', 'value' => $data['user_details'][0]['name']]) ?>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Email</label>
                    <?php echo form_input(['name' => 'email_addr', 'class' => 'form-control', 'value' =>  $data['user_details'][0]['email_addr']])?>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Phone Number</label>
                    <?php echo form_input(['name' => 'mobile_number', 'class' => 'form-control', 'value' =>  $data['user_details'][0]['mobile_number']])?>
                </div>
                    <?php echo form_input(['name' => 'id', 'value' => $data['user_details'][0]['id'],'type'=> 'hidden'])?>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              <?php echo form_close(); ?>
        </div>
    </div>
  </div>
</div>


<!-- Modal for Add address -->
<div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Address</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body">
          <?php echo form_open_multipart('index.php/home/add_new_address/'.$this->uri->segment(2));?>
            <div class="form-group">
                <label for="exampleInputPassword1">Zip code</label>
                <?php echo form_input(['name' => 'zip_code', 'class' => 'form-control', 'value' =>  set_value('zip_code'), 'required' =>'required']) ?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">City</label>
                <?php echo form_input(['name' => 'city', 'class' => 'form-control', 'value' =>  set_value('city'),'required' =>'required'])?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Landmark</label>
                <?php echo form_input(['name' => 'landmark', 'class' => 'form-control', 'value' =>  set_value('landmark'),'required' =>'required'])?>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Address</label>
                <?php echo form_input(['name' => 'address', 'class' => 'form-control', 'value' => set_value('address'),'required' =>'required'])?>
            </div>
            <div class="form-group">
                <?php echo form_input(['name' => 'id', 'value' => $data['user_data'][0]['id'],'type'=> 'hidden'])?>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
          <?php echo form_close(); ?>
        </div>
    </div>
  </div>
</div>

<script>
function show_order_details(id){
  alert(id)
}
</script>

<script type="text/javascript">
    function deleteaddress(id){
        if (confirm('Are you sure,  After click ok Your Address will be deleted from Database')) {
            location.replace("<?php echo base_url('/home/delete_address/');?>"+id);
        }
    }

     function deleteWishProduct(id){
      var message = "Are you sure you want to delete this product from the wishlist";
      var url = 'user/deleteWishProduct/'+id;
      confirm(url);
      window.location.href = global_config.base_url+url;
    }

    function deletecartproduct(id){
      var message = "Are you sure you want to delete this product from the cart";
      var url = 'user/deletecartproduct/'+id;
      confirm(message);
      window.location.href = global_config.base_url+url;
    }
</script>
<style>
  .tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 18%;
    margin-bottom: 10%;
  }

  .tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
  }

  .tab button:hover {
    background-color: #ddd;
  }

  .tab button.active {
    background-color: #ccc;
  }

  .tabcontent {
    float: left;
    padding: 0px 12px;
    border-right: 1px solid #ccc;
    border-top: 1px solid #ccc;
    width: 82%;
    border-left: none;
  }

  @media only screen and (max-width: 600px) {
    .tabcontent {
      width: 75% !important;
    }
    .tab {
      width: 25% !important;
    }
  }
</style>
<div style="clear:both"></div>



<?php $this->load->view('template/footer');?>
