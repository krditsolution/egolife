<?php $this->load->view('template/header'); ?>
<main class="site-content site-content--login">
    <section class="pagetitle-section d-none">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-lg-6 text-center text-lg-start">
                    <h1 class="pagetitle">Login</h1>
                </div>
                <div class="col-12 col-lg-6 text-center text-lg-end">
                    <nav aria-label="breadcrumb" class="d-inline-block">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item fw-semibold"><a href="index.html">Home</a></li>
                            <li class="breadcrumb-item fw-semibold active" aria-current="page"><span>Login</span></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="user-action-section section--py bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10 col-xl-10 col-xxl-9">
                    <div class="user-action-forms-wrapper bg-white shadow h-100">
                        <div class="row g-0 align-items-center">
                            <div class="col-12 col-lg-6">
                                <div class="form-wrapper form-wrapper--login">
                                    <h3 class="text-dark fw-bold mb-2 pt-3 pt-lg-0 text-center text-lg-start">Login</h3>
                                    <p class="text-para mb-4 text-center text-lg-start">Welcome back! Login to your account.</p>
                                    <?php echo form_open_multipart('user/login_user', array('id' => 'loginForm','class' => 'form login-form pt-2'));?>
                                        <div class="form__field"><input name="email_addr" class="form__input" placeholder="Email Address*" required></div>
                                        <div class="form__field mb-4"><input type="password" name="password" class="form__input" placeholder="Password*" required><a href="javascript:void(0)" class="button--show-password p-1"><i class="bi bi-eye"></i></a></div><button type="submit" class="btn btn-primary d-block w-100">Login</button>
                                    <?php echo form_close()?>
                                    <p class="text--para mt-3 mb-0 text-center pb-3 pb-lg-0">Don't have an account? <a href="javascript:void(0)" class="btn--to-register fw-semibold text-decoration-underline text--highlight">Register Now</a> or<br>Forgot you password? <a href="javascript:void(0)" class="btn--forgot-password fw-semibold text-decoration-underline">Click Here</a></p>
                                </div>
                            </div>
                            <div class="col-12 col-lg-6">
                                <div class="form-wrapper form-wrapper--register">
                                    <h3 class="text-dark fw-bold mb-2 pt-3 pt-lg-0 text-center text-lg-start">Register</h3>
                                    <p class="text-para mb-4 text-center text-lg-start">Fill in your details below to create an egolife capital account.</p>
                                    <?php echo form_open_multipart('user/user_register', array('id' => 'registerForm','class' => 'form register-form'));?>
                                        <div class="form__field"><input name="name" class="form__input" placeholder="Fullname*" required></div>
                                        <div class="form__field"><input name="email_addr" class="form__input" placeholder="Email Address*" required></div>
                                        <div class="form__field"><input name="mobile_number" class="form__input" placeholder="Phone Number*" required></div>
                                        <div class="form__field"><input type="password" name="r_password" class="form__input" placeholder="Password*" required><a href="javascript:void(0)" class="button--show-password p-1"><i class="bi bi-eye"></i></a></div>
                                        <p class="mb-4 text-lightText text-xs text-center">By signing up you agree to our <a href="#" class="primary-link text-xs text-decoration-underline fw-semibold">Terms's &amp; Condition</a> and <a href="#" class="primary-link text-xs text-decoration-underline fw-semibold">Privacy Service</a></p><button type="submit" class="btn btn-primary d-block w-100">Register</button>
                                    <?php echo form_close()?>
                                    <p class="text-center text--para mb-0 mt-4 pb-3 pb-lg-0">Already have an account? <a href="javascipt:void(0)" class="btn--to-login text-decoration-underline text--highlight fw-semibold">Click here</a> to Login</p>
                                </div>
                            </div>
                        </div>
                        <div class="sliding-block"><img src="<?php echo base_url()?>assets/images/login-side-bg.webp" class="w-100 h-100 fit-cover" alt="background"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php $this->load->view('template/footer'); ?>

<script>
    $(document).ready(function() {
        $("#user_register").validate({
            rules: {
                mobile_number: {
                    required: true,
                    minlength: 10,
                    maxlength: 10,
                }
            }
        });
    });
</script>

<style>
    .alert {
        position: relative;
        padding: 12px 20px;
        padding: 0.75rem 1.25rem;
        margin-bottom: 16px;
        margin-bottom: 1rem;
        border: 1px solid transparent;
        border-radius: 0.25rem;
    }

    .alert-danger {
        color: #fe1200;
        background-color: #ffffff;
        border-color: #fe0e00;
    }

    .alert-success {
        color: #285b2a;
        background-color: #fff;
        border-color: #285b2a;
    }
</style>