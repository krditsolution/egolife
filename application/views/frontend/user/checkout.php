<?php $this->load->view('template/header');
?>
<main class="site-content site-content--checkout">
    <!-- page title -->
    <section class="pagetitle-section d-flex align-items-center justify-content-center">
        <div class="pagetitle-block py-4 border-top border-bottom">
            <h3 class="text-primary py-2 text-uppercase">Checkout</h3>
        </div>
    </section>

    <section class="section--py">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-8 mb-5 mb-lg-0 pe-lg-5">
                    <div class="checkout-steps mx-4 mx-sm-0">
                        <div class="checkout-step__header d-flex justify-content-between position-relative" data-stepcompleted="1">
                            <div class="checkout-step active">
                                <span class="text-xs checkout-step__heading-prefix">Login</span>
                                <span class="checkout-step__step-count">1</span>
                            </div>
                            <div class="checkout-step">
                                <span class="text-xs checkout-step__heading-prefix">Add Address</span>
                                <span class="checkout-step__step-count">2</span>
                            </div>
                            <div class="checkout-step">
                                <span class="text-xs checkout-step__heading-prefix">Payment</span>
                                <span class="checkout-step__step-count">3</span>
                            </div>
                        </div>
                        <div class="checkout-step__content mt-4">
                            <div class="address-container">
                                <ul class="address-saved-list row">
                                    <?php foreach($data['address'] as $address):?>
                                        <li class="address-saved-list__item col-12 col-sm-6 col-xl-4 <?php if($address['is_default']>0){echo "selected";}?>">
                                            <div class="p-4 bg--light rounded border border-1">
                                                <p class="text-xs text-dark m-0 p-2">
                                                    <?php echo $address['address']." , ".$address['city']." , ".$address['zip_code']." , ".$address['city']." , ".$address['state'].", Near: ".$address['landmark']?>
                                                </p>
                                                <a href="<?php echo base_url('user/make_default_address/').$address['id']?>" class="address-saved-list__item--select-btn"><i class="bi bi-check-circle-fill"></i></a>
                                            </div>
                                        </li>
                                    <?php endforeach;?>
                                    <div class="col-12 col-sm-6 col-xl-4 mt-3 mt-sm-0 text-center text-sm-start">
                                        <a class="d-inline-block w-auto address-container__btn--add-adress fw-bold text-decoration-underline" data-bs-toggle="offcanvas" href="#addAddressOffCanvas" role="button" aria-controls="offcanvasExample">Add Address<span class="ms-1"><i class="bi bi-plus"></i></span></a>
                                    </div>
                                </ul>
                               
                                    <?php if(!empty($data['address'])):?>
                                        <div class="text-end mt-5">
                                            <a href="<?php echo base_url('payment/index')?>" class="address-container__button--proceed-to-payment btn btn-secondary d-inline-block">Proceed To Payment<i class="bi bi-arrow-right ms-2"></i></a>
                                        </div>
                                    <?php else:?>
                                        <p class="text-danger mt-5 text-center fw-semibold"> Address Not Found! :( <br> Please Add A Address! </p>
                                    <?php endif;?>
                            </div>
                            <div class="payment-methods-container mt-5">
                                <div class="d-flex flex-wrap justify-content-center justify-content-sm-between align-items-center">
                                    <a href="javascript:void(0)" class="btn btn-primary payment-method__button--online-payment">Pay Now</a>
                                    <a href="javascript:void(0)" class="text-secondary payment-method__button--cod ms-2 mt-3 ms-sm-0 mt-sm-0">Proceed With Cash On Devlivery<span class="ms-2"><i class="bi bi-arrow-right"></i></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-4 p-sm-0 px-lg-3">
                    <div class="order-summary border border-1 rounded">
                        <h5 class="text-dark order-summary__heading mb-4 fw--semibold">Order Summary</h5>
                        <ul class="order-summary__product-preview-container mt-1">
                            
                            <?php foreach($this->cart->contents() as $key=>$value):?>
                                <?php $image = json_decode($value['img'],TRUE);?>
                                <li class="product-preview row">
                                    <div class="col-2 p-0">
                                        <a href="#" class="product-preview--link d-inline-block">
                                            <img src="<?php echo base_url('assets/images/products/'.$image['images_0'])?>" alt="product1" width="40" />
                                        </a>
                                    </div>
                                    <div class="col-7 p-0">
                                        <p class="text-dark text--sm">
                                            <span class="product-preview__title"><?php echo $value['name']?></span>
                                            <span class="product-preview__added-count-value text--para">x <?php echo $value['qty']?></span>
                                        </p>
                                    </div>
                                    <div class="col-3 text-end p-0">
                                        <h6 class="text--primary m-0 fw--semibold">&#8377; <?php echo $value['subtotal']?></h6>
                                    </div>
                                </li>

                                <?php 
                                    $total_amount[] = $value['subtotal'];
                                ?>
                            <?php endforeach;?>
                            
                        </ul>
                        <div class="order-summary__subtotal-container d-flex justify-content-between align-items-center mb-3">
                            <p class="text--sm text-dark m-0 fw-bold">Subtotal</p>
                            <h6 class="text--primary fw--semibold m-0">&#8377; <?php echo $product_amount = array_sum($total_amount);?></h6>
                        </div>
                        <div class="order-summary__subtotal-container d-flex justify-content-between align-items-center mb-3">
                            <p class="text--sm text-dark m-0 fw-bold">GST (18%)</p>
                            <h6 class="text--primary fw--semibold m-0">&#8377; <?php echo $gst = ($product_amount* vivaTaxCalculator());?></h6>
                        </div>
                        <div class="order-summary__subtotal-container cart__subtotal-container--shipping d-flex justify-content-between align-items-center mb-3">
                            <p class="text--sm text-dark m-0 fw-bold">Shipping Charge</p>
                            <h6 class="text--primary fw--semibold m-0"><span class="shipping-charge__prefix text-xs bg--secondary px-2 py-1 me-1 fw-bold border-radius text--light"></span><span class="shipping-charge__value">&#8377; <?php echo $shipping_charge = vivaShippingCharges();?> </span></h6>
                        </div>
                        <div class="coupan-code-input-container position-relative mb-3 d-none">
                            <input type="text" name="coupancode" id="applyCoupanCode" placeholder="Apply coupan code here..." />
                            <img src="<?php echo base_url()?>assets/images/discount-icon.png" alt="coupan-icon" width="24" />
                        </div>
                        <div class="order-summary__total-container d-flex justify-content-between align-items-center mb-0 bg-primary">
                            <p class="h5 text-white m-0 fw-bold">Total</p>
                            <h5 class="h5 text-white m-0 fw-bold">&#8377; <?php echo $product_amount+$gst+$shipping_charge;?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<!-- add address offcanvas -->
<div class="offcanvas offcanvas-start" tabindex="-1" id="addAddressOffCanvas" aria-labelledby="offcanvasLabel">
    <div class="offcanvas-header justify-content-lg-end">
        <button type="button" class="btn-close border-0 bg-transparent text-danger h5" data-bs-dismiss="offcanvas" aria-label="Close"><i class="bi bi-x-lg"></i></button>
    </div>
    <div class="offcanvas-body">
        <form action="<?php echo base_url('user/user_add_address');?>" class="add-address-form form" method="post" id="addAddressForm">
            <div class="row">
                <div class="form__field col-12">
                    <label for="Add Address">Address<span class="text--error">*</span></label>
                    <input type="text" class="add-address-form__input-address form__input" name="address" />
                </div>
                <div class="form__field col-12">
                    <label for="Add Address">Door / Flat No.<span class="text--error">*</span></label>
                    <input type="text" class="add-address-form__input-door-flat-no form__input" required name="door_address" />
                </div>
                <div class="form__field col-6">
                    <label for="Add Address">City<span class="text--error">*</span></label>
                    <input type="text" class="add-address-form__input-city form__input" name="city" required />
                </div>
                <div class="form__field col-6">
                    <label for="Add Address">Pin Code<span class="text--error">*</span></label>
                    <input type="number" class="add-address-form__input-pin form__input" name="zip_code" required />
                </div>
                <div class="form__field col-12">
                    <label for="Add Address">Landmark<span class="text--error">*</span></label>
                    <input type="text" class="add-address-form__input-pin form__input" name="landmark" required />
                </div>
                <div class="form__field col-12">
                    <label for="Add Address">State<span class="text--error">*</span></label>
                    <input type="text" class="add-address-form__input-door-flat-no form__input" value="West Bengal" name="state"required />
                    <input type="hidden" class="add-address-form__input-door-flat-no form__input" value="<?php echo $this->session->userdata('user_id')?>" name="user_id" />
                    <input type="hidden" class="add-address-form__input-door-flat-no form__input" value="<?php echo current_url();?>" name="origin_url" />
                </div>
                <div class="col-12 mt-2">
                    <button class="d-block btn btn-primary w-100">Save address &amp; proceed</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php $this->load->view('template/footer');?>

<script>  
    $(document).ready (function () {  
        $("#addAddressForm").validate({
            rules: {
                zip_code:{
                    required: true,
                    minlength: 6,
                    maxlength: 6,
                }
            }
        });  
    }); 
</script>