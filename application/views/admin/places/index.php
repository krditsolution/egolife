<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-6 col-xs-6 hidden-xs">
            		<h4 class="c-grey-900 mB-20">All Vendors (Suppliers)</h4>
            	</div>
            	<!-- <div class="col-md-6 col-xs-12 text-right">
            		<a href="<?php  ?>" class="btn btn-primary mB-20" roll="button"> <span class="c-orange-500 ti-plush"></span> Take Order On Phone Call</a>
            	</div> -->
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <table id="dataTable" class="table table-striped table-responsive-md table-responsive-xs table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><b>#</b></th>
                                    <th style="width: 105px !important;">Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>User Type</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                <th><b>#</b></th>
                                    <th style="width: 105px !important;">Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>User Type</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                               
                                    <?php $sr = 1;
                                        foreach($data['get_places'] as $data){ ?>
                                            <tr>
                                                <td><?php echo $sr ;?></td>
                                                <td><?php echo $data['name'] ?></td>
                                                <td><?php echo $data['mobile_number'] ?></td>
                                                <td><?php echo $data['email_addr']?></td>
                                                <td><?php echo $data['user_type']?></td>
                                                <td><?php echo $data['date_time'] ?></td>
                                                <td>
                                                    <div class="dropdown show">
                                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </a>
                                                        <?php if($this->session->userdata('admin_id') != $data['id']):?>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                                <a style="cursor:pointer;" class="dropdown-item" onclick="blockUser(<?php echo $data['id']?>,0)"> <i class="fa fa-trash mr-2"> </i> Block User</a>
                                                                <!-- <a style="cursor:pointer;" class="dropdown-item" onclick="unBlockUser(<?php //echo $data['id']?>,1)" > <i class="fa fa-check mr-2"> </i> Unblock User</a> -->
                                                            </div>
                                                        <?php endif;?>
                                                    </div>
                                                </td>
                                            </tr>
                                       <?php $sr++; }
                                    ?>
                            </tbody>
                        </table>
                        <div class="text-right pagination_btn">
						    <?= $this->pagination->create_links(); ?>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php $this->load->view('admin/template/footer');?>

<script>
    function blockUser(id, status){
        var message = "Are you sure you want to block this User"
        if(confirm(message)){
            window.location.href = "<?php echo base_url('admin_user/block_unblock_user/')?>"+id+'/'+status;
        }
    }

    // function unBlockUser(id, status){
    //     var message = "Are you sure you want to block this User"
    //     if(confirm(message)){
    //         window.location.href = "<?php echo base_url('admin_user/block_unblock_user/')?>"+id+'/'+status;
    //     }
    // }
</script>