<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>


<main class="main-content bgc-grey-100">
        <div id="mainContent">
            <div class="row gap-20 masonry pos-r">
                <div class="masonry-sizer col-md-6"></div>
                <div class="masonry-item col-md-12">
                    <div class="bgc-white p-20 bd">
                        <h6 class="c-grey-900">
                           Product Upload
                        </h6>

                        <div class="mT-30">
                            <?php echo form_open_multipart('admin_product/add_category');?>
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label >Name</label>
                                        <?php echo form_input(['name' => 'name', 'class' => 'form-control', 'required'=> 'required', 'value' => set_value('name')]) ?>
                                    </div>
                                    
                                    <div class="form-group col-md-4 col-xs-12">
                                        <label for="inputState">Image </label>
                                        <?php echo form_input(['name' => 'userfile', 'type' => 'file','required'=> 'required', 'class' => 'form-control']) ?>
                                    </div>
                                </div>

                                <div class="form-row mt-5">
                                    <br><br><br><br><br><br>
                                    <br><br><br><br><br><br>
                                    <div class="form-group col-md-2 w-100">
                                        <button type="submit" class="btn btn-outline-primary w-100">Add Category</button>
                                    </div>
                                    <div class="form-group col-md-2 w-100">
                                        <a href="<?php echo base_url('admin_product/category')?>" class="btn btn-outline-danger w-100">Cancel</a>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </main>

<?php $this->load->view('admin/template/footer');?>