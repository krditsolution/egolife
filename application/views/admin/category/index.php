<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>


<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-6 col-xs-6 hidden-xs">
            		<h4 class="c-grey-900 mB-20">All Category</h4>
            	</div>
            	<div class="col-md-6 col-xs-12 text-right">
            		<a href="<?php echo base_url('admin_product/add_category') ?>" class="btn btn-primary mB-20" roll="button"> <span class="c-orange-500 ti-plush"></span> Add Category</a>
            	</div>
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <table id="dataTable" class="table table-striped table-responsive-xs table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <th><b>#</b></th>
                                	<th>Image</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th><b>#</b></th>
                                	<th>Image</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                               
                                    <?php 
                                        $sr = 1;
                                        foreach($data['product_category'] as $products){ ?>
                                            <tr>
                                                <td><?php echo $sr ;?></td>
                                                <td  width="85"> <a href=""><img class="img-responsive" src="<?php echo base_url('assets/images/category/').$products['img'] ;?>" height = '50px'></a></td>
                                                <td><?php echo $products['name'] ;?></td>
                                                <td>
                                                    <div class="dropdown show">
                                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </a>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <a style="cursor: pointer;" class="dropdown-item"  onclick="deleteCategory(<?php echo $products['id'];?>)"> <i class="fa fa-trash mr-2"> </i> Delete</a>
                                                            <a style="cursor: pointer;" class="dropdown-item" href="<?php echo base_url('admin_product/edit_category/').$products['id']; ?>"> <i class="fa fa-pencil mr-2"> </i>  Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                       <?php $sr++;  }
                                     ?>
                            </tbody>
                        </table>
                        <div class="text-right pagination_btn">
						    <?= $this->pagination->create_links(); ?>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $this->load->view('admin/template/footer');?>

<script>
    function deleteCategory(id){
        if (confirm('Are You Sure You want to delete?')) {
            window.location.href = "<?php echo base_url('admin_product/delete_category/')?>"+id;
        }
    }
</script>