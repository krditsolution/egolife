<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>



<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="row gap-20 masonry pos-r">
            <div class="masonry-sizer col-md-6"></div>
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h6 class="c-grey-900">
                        Category Upload
                    </h6>
                    <div class="mT-30">
                        <?php echo form_open_multipart('admin_product/edit_category/'.$data['category_by_id'][0]['id']);?>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Name</label>
                                    <?php echo form_input(['name' => 'name', 'class' => 'form-control', 'value' => $data['category_by_id'][0]['name']]) ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-2 w-100">
                                    <button type="submit" class="btn btn-outline-primary w-100">Upload</button>
                                </div>
                                <div class="form-group col-md-2 w-100">
                                    <a href="<?php echo base_url('admin_product/category');?>" class="btn btn-outline-danger w-100">Cancel</a>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>
<style>
    
/* css for the switcher */
html,
body {
  height: 100%;
  background-color: #D1D5DB;
}

.switch-wrapper {
  display: grid;
  place-content: center;
  min-height: 100%;
}

.switch {
  display: none;
}

.switch + div {
  width: 48px;
  height: 24px;
  border-radius: 12px;
  background-color: #ff1313;
  transition: background-color 200ms;
  cursor: pointer;
}

.switch:checked + div {
  background-color: #00a850;
}

.switch + div > div {
  width: 24px;
  height: 24px;
  border-radius: 23px;
  background-color: #fff;
  transition: transform 250ms;
  pointer-events: none;
}

.switch:checked + div > div {
  transform: translateX(28px);
}
</style>


<?php $this->load->view('admin/template/footer');?>