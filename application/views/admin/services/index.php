<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
            	<div class="col-md-6 col-xs-6 hidden-xs">
            		<h4 class="c-grey-900 mB-20">All Services</h4>
            	</div>
            	<div class="col-md-6 col-xs-12 text-right">
            		<a href="<?php echo base_url('admin_services/add') ?>" class="btn btn-primary mB-20" roll="button"> <span class="c-orange-500 ti-plush"></span> Add Services</a>
            	</div>
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <table id="dataTable" class="table table-striped table-responsive-xs table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                <th><b>#</b></th>
                                	<th>Image</th>
                                    <th>Name</th>
                                    <th>Url</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th><b>#</b></th>
                                	<th>Image</th>
                                    <th>Name</th>
                                    <th>Url</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>
                               
                                    <?php 
                                        $sr = 1;
                                        foreach($data['services'] as $products){ 
                                            $image = json_decode($products['img'], TRUE);
                                        ?>
                                            <tr>
                                                <td><?php echo $sr ;?></td>
                                                <td  width="85"> <a href=""><img class="img-responsive" src="<?php echo base_url('assets/images/services/').$image['images_0'] ;?>" height = '50px'></a></td>
                                                <td><?php echo $products['name'] ;?></td>
                                                <td><?php echo $products['url'] ;?></td>
                                                
                                                <td><?php echo date('Y-m-d', strtotime($products['date_time']));?></td>
                                                <td>
                                                    <div class="dropdown show">
                                                        <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            Action
                                                        </a>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                                            <a style="cursor: pointer;" class="dropdown-item"  onclick="deleteProduct(<?php echo $products['id'];?>)"> <i class="fa fa-trash mr-2"> </i> Delete</a>
                                                            <a style="cursor: pointer;" class="dropdown-item" href="<?php echo base_url('admin_services/edit_service/').$products['id']; ?>"> <i class="fa fa-pencil mr-2"> </i>  Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                       <?php $sr++;  }
                                     ?>
                            </tbody>
                        </table>
                        <div class="text-right pagination_btn">
						    <?= $this->pagination->create_links(); ?>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<style type="text/css">
	.dataTables_info#dataTable_info,
	.dataTables_paginate#dataTable_paginate{
		display: none !important;
	}

	span.btn-info a,
	button.btn-info a{
		color: #fff !important;
	}

</style>

<script type="text/javascript">
    function deleteProduct(id){
        if (confirm('Are you sure You want to delete?')) {
            location.replace("<?php echo base_url('admin_services/delete_service/');?>"+id);
        }
    }
</script>
<?php $this->load->view('admin/template/footer');?>
