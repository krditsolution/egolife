<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>

<link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;600;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/nic_edit.js')?>"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="row gap-20 pos-r">
            <div class="masonry-sizer col-md-6"></div>
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h6 class="c-grey-900">
                        Add Services
                    </h6>

                    <div class="mT-30">
                        <?php echo form_open_multipart('admin_services/add_services_init', array('id' => 'add_product'));?>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label >Name</label>
                                    <?php echo form_input(['name' => 'name', 'required' => 'required', 'class' => 'form-control', 'value' => set_value('name')]) ?>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label>Url</label>
                                    <?php echo form_input(['name' => 'url', 'required' => 'required', 'class' => 'form-control', 'value' => set_value('selling_price')]) ?>
                                </div>

                                <hr style="margin:40px; border-top:1px solid #ccc; width:100%; clear:both">
                                
                                <div class="form-group col-md-6 col-xs-12">
                                    <label for="inputAddress2">Image*</label>
                                    <?php echo form_input(['name' => "userfile[]", "multiple"=>"multiple", 'type' => 'file', 'class' => 'form-control','required' => 'required']) ?>
                                </div>
                                
                                <hr style="margin:40px; border-top:1px solid #ccc; width:100%; clear:both">

                                <div class="form-group col-md-9">
                                    <label for="inputEmail4">Description</label>
                                    <textarea name="description" required class="form-control" height="150"></textarea>
                                </div>

                                <br><br><br><br><br><br>
                                <div class="form-group col-md-8 w-100 mt-5"></div>
                                <div class="form-group col-md-2 w-100">
                                    <button type="submit" class="btn btn-outline-primary w-100">Add Services</button>
                                </div>
                                <div class="form-group col-md-2 w-100">
                                    <a href="<?php echo base_url('admin_services/index')?>" class="btn btn-outline-danger w-100">Cancel</a>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $this->load->view('admin/template/footer');?>
<script>
    $(document).ready(function(){
      $("#add_product").validate({});
    });
</script>