<?php $this->load->view('admin/template/header');?>
<?php $this->load->view('admin/template/side_panel');?>

<script type="text/javascript" src="<?php echo base_url('assets/admin/js/nic_edit.js')?>"></script>
<script type="text/javascript">
	bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
</script>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="row gap-20 pos-r">
            <div class="masonry-sizer col-md-6"></div>
            <div class="masonry-item col-md-12">
                <div class="bgc-white p-20 bd">
                    <h6 class="c-grey-900">
                       Edite Software Details
                    </h6>
                    
                    <div class="mT-30">
                        <?php echo form_open_multipart('admin_services/do_edit_services', array('id' => 'edit_product'));?>
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label >Name</label>
                                    <?php echo form_input(['name' => 'name', 'required' => 'required', 'class' => 'form-control', 'value' => $data['product'][0]['name']]) ?>
                                    <?php echo form_input(['name' => 'id','type' => 'hidden', 'value' => $data['product'][0]['id']]) ?>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Url</label>
                                    <?php echo form_input(['name' => 'url', 'required' => 'required', 'value' => $data['product'][0]['url'], 'class' => 'form-control']) ?>
                                </div>

                                <hr style="margin:40px; border-top:1px solid #ccc; width:100%; clear:both">
                                <div class="form-group col-md-9">
                                    <label for="inputEmail4">Description</label>
                                    <textarea name="description" required class="form-control" height="150">
                                    <?php echo $data['product'][0]['description']; ?>
                                    </textarea>
                                </div>
                                <hr style="margin:40px; border-top:1px solid #ccc; width:100%; clear:both">
                                
                                <div class="form-group col-md-6 col-xs-12">
                                    <label for="inputAddress2">Image*</label>
                                    <?php echo form_input(['name' => "userfile[]", "multiple"=>"multiple", 'type' => 'file', 'class' => 'form-control']) ?>
                                </div>
                            </div>

                            <div class="form-row">
                                <br><br><br><br><br><br>
                                <div class="form-group col-md-8 w-100 mt-5"></div>
                                <div class="form-group col-md-2 w-100">
                                    <button type="submit" class="btn btn-outline-primary w-100">Update</button>
                                </div>
                                <div class="form-group col-md-2 w-100">
                                    <a href="<?php echo base_url('admin_softwars');?>" class="btn btn-outline-danger w-100">Cancel</a>
                                </div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php $this->load->view('admin/template/footer');?>

<script>
    $(document).ready(function(){

        $("#edit_product").validate({});

        let multiselect_block = document.querySelectorAll(".multiselect_block");
        multiselect_block.forEach(parent => {
            let label = parent.querySelector(".field_multiselect");
            let select = parent.querySelector(".field_select");
            let text = label.innerHTML;
            select.addEventListener("change", function(element) {
                let selectedOptions = this.selectedOptions;
                label.innerHTML = "";
                for (let option of selectedOptions) {
                    let button = document.createElement("button");
                    button.type = "button";
                    button.className = "btn_multiselect";
                    button.textContent = option.value;
                    button.onclick = _ => {
                        option.selected = false;
                        button.remove();
                        if (!select.selectedOptions.length) label.innerHTML = text
                    };
                    label.append(button);
                }
            })
        })
    });
</script>

<style>
    input,
    select.common_select,
    option{
        height: 50px !important;
    }

.form_label input,
.field_multiselect {
    position: relative;
    width: 100%;
    display: block;
    min-height: 50px;
    border: 1px solid #ced4da;
    box-sizing: border-box;
    border-radius: 8px;
    padding: 15px 40px 8px 10px;
    margin-top: 25px;
}
.form_label input::placeholder,
.field_multiselect::placeholder {
  color: #a8acc9;
}
.form_label input:hover,
.field_multiselect:hover {
  box-shadow: 0 0 2px rgba(0, 0, 0, 0.16);
}
.form_label input:focus,
.field_multiselect:focus {
  box-shadow: 0 0 2px rgba(0, 0, 0, 0.16);
}

.field_multiselect_help {
  position: absolute;
  max-width: 100%;
  background-color: #fff;
  top: -48px;
  left: 0;
  opacity: 0;
}

.field_multiselect {
  padding-right: 60px;
  display: flex;
    flex-wrap: wrap;
}
.field_multiselect:after {
  content: "";
  position: absolute;
  right: 14px;
  top: 15px;
  width: 6px;
  height: 16px;
  background: url("data:image/svg+xml,%3Csvg width='6' height='16' viewBox='0 0 6 16' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M3 0L6 5H0L3 0Z' fill='%23A8ACC9'/%3E%3Cpath d='M3 16L6 11H0L3 16Z' fill='%23A8ACC9'/%3E%3C/svg%3E") 50% 50% no-repeat;
}

.multiselect_block {
  position: relative;
  width: 100%;
}

.field_select {
  position: absolute;
  top: calc(100% - 2px);
  left: 0;
  width: 100%;
  border: 2px solid #cdd6f3;
  border-bottom-right-radius: 2px;
  border-bottom-left-radius: 2px;
  box-sizing: border-box;
  outline-color: #cdd6f3;
  z-index: 6;
}

.field_select[multiple] {
  overflow-y: auto;
}

.field_select option {
  display: block;
  padding: 8px 16px;
  width: calc(370px - 32px);
  cursor: pointer;
}
.field_select option:checked {
  background-color: #eceff3;
}
.field_select option:hover {
  background-color: #d5e8fb;
}

.field_multiselect button {
  position: relative;
  padding: 7px 34px 7px 8px;
  background: #ebe4fb;
  border-radius: 4px;
  margin-right: 9px;
  margin-bottom: 10px;
}
.field_multiselect button:hover, .field_multiselect button:focus {
  background-color: #dbd1ee;
}
.field_multiselect button:after {
  content: "";
  position: absolute;
  top: 7px;
  right: 10px;
  width: 16px;
  height: 16px;
  background: url("data:image/svg+xml,%3Csvg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M19.4958 6.49499C19.7691 6.22162 19.7691 5.7784 19.4958 5.50504C19.2224 5.23167 18.7792 5.23167 18.5058 5.50504L12.5008 11.5101L6.49576 5.50504C6.22239 5.23167 5.77917 5.23167 5.50581 5.50504C5.23244 5.7784 5.23244 6.22162 5.50581 6.49499L11.5108 12.5L5.50581 18.505C5.23244 18.7784 5.23244 19.2216 5.50581 19.495C5.77917 19.7684 6.22239 19.7684 6.49576 19.495L12.5008 13.49L18.5058 19.495C18.7792 19.7684 19.2224 19.7684 19.4958 19.495C19.7691 19.2216 19.7691 18.7784 19.4958 18.505L13.4907 12.5L19.4958 6.49499Z' fill='%234F5588'/%3E%3C/svg%3E") 50% 50% no-repeat;
  background-size: contain;
}

.multiselect_label {
  position: absolute;
  top: 1px;
  left: 2px;
  width: 100%;
  height: 44px;
  cursor: pointer;
  z-index: 3;
}

.field_select {
  display: none;
}

input.multiselect_checkbox {
  position: absolute;
  right: 0;
  top: 0;
  width: 40px;
  height: 40px;
  border: none;
  opacity: 0;
}

.multiselect_checkbox:checked ~ .field_select {
  display: block;
}

.multiselect_checkbox:checked ~ .multiselect_label {
  width: 40px;
  left: initial;
  right: 4px;
  background: #ffffff url("data:image/svg+xml,%3Csvg width='24' height='24' viewBox='0 0 24 24' fill='none' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath fill-rule='evenodd' clip-rule='evenodd' d='M19.4958 6.49499C19.7691 6.22162 19.7691 5.7784 19.4958 5.50504C19.2224 5.23167 18.7792 5.23167 18.5058 5.50504L12.5008 11.5101L6.49576 5.50504C6.22239 5.23167 5.77917 5.23167 5.50581 5.50504C5.23244 5.7784 5.23244 6.22162 5.50581 6.49499L11.5108 12.5L5.50581 18.505C5.23244 18.7784 5.23244 19.2216 5.50581 19.495C5.77917 19.7684 6.22239 19.7684 6.49576 19.495L12.5008 13.49L18.5058 19.495C18.7792 19.7684 19.2224 19.7684 19.4958 19.495C19.7691 19.2216 19.7691 18.7784 19.4958 18.505L13.4907 12.5L19.4958 6.49499Z' fill='%234F5588'/%3E%3C/svg%3E") 50% 50% no-repeat;
}

.multiselect_checkbox:checked ~ .field_multiselect_help {
  opacity: 1;
}

@media (max-width: 400px) {
  .field_multiselect_help {
    display: none;
  }
}
</style>