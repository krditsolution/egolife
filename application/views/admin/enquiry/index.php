<?php $this->load->view('admin/template/header'); ?>
<?php $this->load->view('admin/template/side_panel'); ?>

<main class="main-content bgc-grey-100">
    <div id="mainContent">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-xs-6 hidden-xs">
                    <h4 class="c-grey-900 mB-20">All Enquiries</h4>
                </div>
                <div class="col-md-12">
                    <div class="bgc-white bd bdrs-3 p-20 mB-20">
                        <table id="dataTable" class="table table-striped table-responsive-md table-responsive-xs table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><b>#</b></th>
                                    <th style="width: 105px !important;">Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>Subject</th>
                                    <th>Services</th>
                                    <th>Address</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th><b>#</b></th>
                                    <th style="width: 105px !important;">Name</th>
                                    <th>Number</th>
                                    <th>Email</th>
                                    <th>Subject</th>
                                    <th>Services</th>
                                    <th>Address</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                            <tbody>

                                <?php $sr = 1;
                                foreach ($data['enquiry'] as $data) { ?>
                                    <tr>
                                        <td><?php echo $sr; ?></td>
                                        <td><?php echo $data['name'] ?></td>
                                        <td><?php echo $data['mobile_number'] ?></td>
                                        <td><?php echo $data['email_addr'] ?></td>
                                        <td><?php echo $data['subject'] ?></td>
                                        <td><?php echo $data['services'] ?></td>
                                        <td><?php echo $data['state']." , ".$data['city'] ?></td>
                                        <td><?php echo $data['message'] ?></td>
                                        <td><?php echo $data['date_time'] ?></td>
                                        <td>
                                        </td>
                                    </tr>
                                <?php $sr++;
                                }
                                ?>
                            </tbody>
                        </table>
                        <div class="text-right pagination_btn">
                            <?= $this->pagination->create_links(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<?php $this->load->view('admin/template/footer'); ?>

<script>
    function blockUser(id, status) {
        var message = "Are you sure you want to block this User"
        if (confirm(message)) {
            window.location.href = "<?php echo base_url('admin_user/block_unblock_user/') ?>" + id + '/' + status;
        }
    }

    // function unBlockUser(id, status){
    //     var message = "Are you sure you want to block this User"
    //     if(confirm(message)){
    //         window.location.href = "<?php echo base_url('admin_user/block_unblock_user/') ?>"+id+'/'+status;
    //     }
    // }
</script>