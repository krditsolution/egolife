<div>
    <div class="sidebar">
        <div class="sidebar-inner">
            <div class="sidebar-logo">
                <div class="peers ai-c fxw-nw">
                    <div class="peer peer-greed 100">
                        <a class="sidebar-link td-n" href="<?php echo base_url('/admin'); ?>" class="td-n">
                            <div class="peer">
                                <div class="logo w-100">
                                    <img class="img-responsive" src="<?php echo base_url('assets/images/logo.png') ?>" alt="" style="padding-top: 10px;height: 55px;">
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <ul class="sidebar-menu scrollable pos-r">
                <li class="nav-item mT-30 active">
                    <a class="sidebar-link" href="<?php echo base_url('/admin') ?>" default><span class="icon-holder"><i class="c-blue-500 ti-home"></i> </span><span class="title">Dashboard</span></a>
                </li>

                <li class="nav-item dropdown"><a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i class="c-red-500 ti-user"></i> </span><span class="title"> Users</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="<?php echo base_url('admin_user/users') ?>"> <i class="c-orange-500 ti-layout-list-thumb"> </i> All Users </a></li>
                    </ul>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="<?php echo base_url('admin_user/suppliers') ?>"> <i class="c-blue-600 fa fa-lg fa-industry"> </i> All Venders </a></li>
                    </ul>
                </li>

                <li class="nav-item"><a class="sidebar-link" href="<?php echo base_url('admin/order') ?>"><span class="icon-holder"><i class="c-green-600 ti-bar-chart"></i> </span><span class="title">Softwere Downloads</span></a></li>

                <li class="nav-item"><a class="sidebar-link" href="<?php echo base_url('admin_enquiry') ?>"><span class="icon-holder"><i class="c-blue-600 ti-bar-chart"></i> </span><span class="title">Enquiries</span></a></li>

                <li class="nav-item dropdown"><a class="dropdown-toggle" href="javascript:void(0);"><span class="icon-holder"><i class="c-purple-800 fa-lg fa fa-wrench"></i> </span><span class="title">Services</span> <span class="arrow"><i class="ti-angle-right"></i></span></a>
                    <ul class="dropdown-menu">
                        <li><a class="sidebar-link" href="<?php echo base_url('admin_softwars/index') ?>"> <i class="c-purple-400 fa fa-desktop fa-lg mr-3"> </i> All Softwers </a></li>

                        <li><a class="sidebar-link" href="<?php echo base_url('admin_services') ?>"> <i class="c-purple-400 ti-book mr-3 fa-lg"> </i> All Services </a></li>
                    </ul>
                </li>

                <li class="nav-item"><a class="sidebar-link" href="<?php echo base_url('admin/order') ?>"><span class="icon-holder"><i class="c-deep-purple-500 ti-book"></i> </span><span class="title">Promo Code</span></a></li>
            </ul>
        </div>
    </div>
</div>

<div class="page-container">
    <div class="header navbar">
        <div class="header-container">
            <ul class="nav-left">
                <li><a id="sidebar-toggle" class="sidebar-toggle" href="javascript:void(0);"><i class="ti-menu"></i></a></li>
                <li class="search-input"><input class="form-control" type="text" placeholder="Search..."></li>
            </ul>
            <ul class="nav-right">
                <li class="dropdown"><a href="#" class="dropdown-toggle no-after peers fxw-nw ai-c lh-1" data-toggle="dropdown">
                        <div class="peer mR-10"><img class="w-2r bdrs-50p" src="<?php echo base_url('assets/images/logo.png') ?>" alt=""></div>
                        <div class="peer"><span class="fsz-sm c-grey-900"><?php echo $this->session->userdata('user'); ?></span></div>
                    </a>
                    <ul class="dropdown-menu fsz-sm">
                        <li><a href="#" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-user mR-10"></i> <span>Profile</span></a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="<?php echo base_url('admin/logout') ?>" class="d-b td-n pY-5 bgcH-grey-100 c-grey-700"><i class="ti-power-off mR-10"></i> <span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>