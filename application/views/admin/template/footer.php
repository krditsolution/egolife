
<footer class="bdT ta-c p-30 lh-0 fsz-sm c-grey-600"><span>Copyright © 2022 Designed by <a href="" target="_blank" title="Colorlib">Kazi Imam Hasan</a>. All rights reserved.</span>
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script> -->
    <!-- <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-23581568-13');
    </script> -->
</footer>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/vendor.js')?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/admin/js/bundle.js')?>"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>

</body>

<!-- <script src="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.jquery.min.js"></script>
<link href="https://cdn.rawgit.com/harvesthq/chosen/gh-pages/chosen.min.css" rel="stylesheet"/> -->

<!-- <script>
  $(document).ready(function(){
    $(".chosen-select").chosen({
      no_results_text: "Oops, nothing found!"
    })
  })
</script> -->

</html>
