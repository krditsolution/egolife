    <!-- Footer -->
    <?php
    // $CI = &get_instance();
    // $get_data_from_table = $CI->Common_model->get_data_from_table('main_category');
    ?>
    <footer class="site-footer bg-dark">
        <div class="site-footer__top">
            <div class="container">
                <div class="menus-wrapper">
                    <div class="menu-block menu-block--about pe-xl-4">
                        <div class="row">
                            <div class="col-12 col-sm-10 col-md-8 col-xl-12">
                                <h6 class="fw-semibold text-white text-uppercase mb-4">About</h6>
                                <p class="text-lightText mb-3">We are a company with its headquarters in Assam, that strives for excellence in the information technology (IT) industry by offering a variety of IT accessory supplies.</p>
                                <form action="#" class="newsletter-form form pt-1">
                                    <div class="form__field d-flex justify-content-between bg-white p-1"><input type="email" name="n_email" class="form__input border-0 form__input--no-focus" placeholder="Your Email*" required><button type="submit" class="btn btn-primary"><i class="fa-solid fa-paper-plane"></i></button></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="menu-block menu-block--services">
                        <h6 class="fw-semibold text-white text-uppercase mb-4">Services</h6>
                        <nav class="footer-menu">
                            <ul class="menu">
                                <li><a href="https://wa.me/919137022985?text=I am Interest to Create my own website. Please help">Website Development</a></li>
                                <li><a href="https://www.psaonline.utiitsl.com/psaonline/showLogin">Pan Card</a></li>
                                <li><a href="">School Service</a></li>
                                <li><a href="https://uidai.gov.in/en/">Aadhaar Solution</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="menu-block menu-block--quick-links">
                        <h6 class="fw-semibold text-white text-uppercase mb-4">Quick Links</h6>
                        <nav class="footer-menu">
                            <ul class="menu">
                                <li><a href="<?php echo base_url() ?>">Home</a></li>
                                <li><a href="<?php echo base_url('about-us') ?>">About</a></li>
                                <li><a href="<?php echo base_url('services') ?>">Services</a></li>
                                <li><a href="<?php echo base_url('softwares') ?>">Softwares</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="menu-block menu-block--need-help">
                        <h6 class="fw-semibold text-white text-uppercase mb-4">Need Help?</h6>
                        <nav class="footer-menu">
                            <ul class="menu">
                                <li><a href="<?php echo base_url('faqs') ?>">FAQs</a></li>
                                <li><a href="<?php echo base_url('contact-us') ?>">Contact</a></li>
                                <li><a href="<?php echo base_url('privacy-policy') ?>">Privacy Policy</a></li>
                                <li><a href="<?php echo base_url('return-policy') ?>">Return Policy</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div class="menu-block menu-block--contact">
                        <h6 class="fw-semibold text-white text-uppercase mb-4">Contact Information</h6>
                        <div class="contact-block d-flex align-items-center mb-3">
                            <div class="contact-block__icon d-flex d-sm-none d-md-flex d-xl-none d-xxl-flex rounded-circle border"><i class="fa-solid fa-phone"></i></div>
                            <div class="contact-block__text ps-3 ps-sm-0 ps-md-3 ps-xl-0 px-xxl-3"><a href="tel:+91 9365336191" class="d-inline-block fw-semibold">+91 9365336191</a>
                                <p class="text-sm mb-0 mt-1">Working 10:00am - 6:00pm</p>
                            </div>
                        </div>
                        <div class="contact-block d-flex align-items-center mb-3 pt-1">
                            <div class="contact-block__icon d-flex d-sm-none d-md-flex d-xl-none d-xxl-flex rounded-circle border"><i class="fa-solid fa-envelope"></i></div>
                            <div class="contact-block__text ps-3 ps-sm-0 ps-md-3 ps-xl-0 px-xxl-3"><a href="mailto:info.egolifecapital.in" class="d-inline-block fw-semibold">info.egolifecapital.in</a>
                                <p class="text-sm mb-0 mt-1">Feel free to send us a query</p>
                            </div>
                        </div>
                        <div class="d-flex align-items-center"><span class="text-sm me-3 text-muted">Follow Us</span>
                            <ul class="social-icons d-flex pt-1">
                                <li><a href="https://www.facebook.com/eGoLifeCapital" target="_blank"><i class="fa-brands fa-facebook-f"></i></a></li>
                                <li><a href="https://instagram.com/egolifecapital.in?igshid=YmMyMTA2M2Y=" target="_blank"><i class="fa-brands fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-footer__bottom">
            <div class="container py-4">
                <div class="row align-items-center py-3 flex-column-reverse flex-lg-row">
                    <div class="col-12 col-lg-6 text-center text-lg-start mb-2 mb-lg-0">
                        <p class="text-muted pt-1 pt-lg-0 d-inline-block w-auto mb-0 copyright-text">Egolife Egovernance Private Limited ©Copyright. All rights reserved. || Created by <a href="https://wa.me/919137022985?text=I am Interest to Create my own website. please help">KRD IT Solution</a></p>
                    </div>
                    <div class="col-12 col-lg-6 ps-lg-5 text-center text-lg-start d-flex justify-content-center justify-content-lg-end">
                        <div class="d-lg-flex align-items-center">
                            <ul class="bottom-menu d-flex pe-3 justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                                <li><a href="<?php echo base_url('terms-condition') ?>">Terms Condition</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="reqAQuoteModal" aria-hidden="true" aria-labelledby="reqAQuoteModalLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content border-0 p-2 p-xl-3">
                    <div class="modal-header border-0">
                        <h1 class="modal-title fs-5" id="exampleModalToggleLabel">&nbsp;</h1><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0">
                        <!-- <form action="<?php ?>" class="form request-quote-form" id="raqForm"> -->
                        <?php echo form_open_multipart(base_url('Admin_enquiry/get_enquiry'), array('class'=> 'form request-quote-form', 'id' => 'raqForm'))?>
                            <div class="row">
                                <div class="form__field col-12 col-lg-6">
                                    <input name="name" class="form__input" placeholder="Full Name*" required>
                                </div>
                                <div class="form__field col-12 col-lg-6">
                                    <input type="email" name="email_addr" class="form__input" placeholder="Email*" required>
                                </div>
                                <div class="form__field col-12 col-lg-6">
                                    <input name="mobile_number" class="form__input" placeholder="Phone*" required>
                                </div>
                                <div class="form__field col-12 col-lg-6">
                                    <select name="state" class="form__input" id="rqfSelectState" required>
                                        <option value="" disabled="disabled" selected="selected">Select State</option>
                                        <?php
                                            $CI = &get_instance();
                                            $get_states = $CI->Common_model->get_data_from_table('states');
                                        ?>
                                        <?php foreach ($get_states as $get_state) : ?>
                                            <option value="<?php print_r($get_state['name']); ?>"><?php print_r($get_state['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form__field col-12 col-lg-6"><select name="city" class="form__input" id="rqfSelectCity" required>
                                        <option value="" disabled="disabled" selected="selected">Select City</option>
                                        <?php
                                        $get_city = $CI->Common_model->get_data_from_table('cities');
                                        ?>
                                        <?php foreach ($get_city as $get_citys) : ?>
                                            <option value="<?php print_r($get_citys['city']); ?>"><?php print_r($get_citys['city']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form__field col-12 col-lg-6"><select name="subject" class="form__input" id="rqfSelectSubject" required>
                                        <option value="" disabled="disabled" selected="selected">Select Subject</option>
                                        <option value="service">Service Related</option>
                                        <option value="others">Others</option>
                                    </select></div>
                                <div class="form__field col-12" style="display: none;">
                                    <select name="services" class="form__input" id="rqfSelectService" required>
                                        <option value="" disabled="disabled" selected="selected">Select Service</option>
                                        <?php
                                        $CI = &get_instance();
                                        $ger_services = $CI->Common_model->get_data_from_table('services');
                                        ?>
                                        <?php foreach ($ger_services as $get_service) : ?>
                                            <option value="<?php print_r($get_service['name']); ?>"><?php print_r($get_service['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form__field col-12"><textarea name="message" class="form__input" placeholder="Message(optional)"></textarea></div>
                                <div class="text-center"><button type="submit" class="btn btn-primary">Submit</button></div>
                            </div>
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="serviceEnquiryModal" aria-hidden="true" aria-labelledby="serviceEnquiryModalLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content border-0 p-2 p-xl-3">
                    <div class="modal-header border-0">
                        <h1 class="modal-title fs-5" id="exampleModalToggleLabel">&nbsp;</h1><button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body border-0">
                        <form action="#" class="form request-quote-form enquiry-form" id="serviceEnquiryForm">
                            <div class="row">
                                <div class="form__field col-12 col-lg-6"><input name="enq_name" class="form__input" placeholder="Full Name*" required></div>
                                <div class="form__field col-12 col-lg-6"><input type="email" name="enq_email" class="form__input" placeholder="Email*" required></div>
                                <div class="form__field col-12 col-lg-6"><input name="enq_phone" class="form__input" placeholder="Phone*" required></div>

                                <div class="form__field col-12 col-lg-6">
                                    <select name="enq_select_state" class="form__input" id="enqSelectState" required>
                                        <option value="" disabled="disabled" selected="selected">Select State</option>
                                        <?php foreach ($get_states as $get_state) : ?>
                                            <option value="<?php print_r($get_state['name']); ?>"><?php print_r($get_state['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form__field col-12 col-lg-6">
                                    <select name="enq_select_city" class="form__input" id="enqSelectCity" required>
                                        <option value="" disabled="disabled" selected="selected">Select City</option>
                                        <?php foreach ($get_city as $get_citys) : ?>
                                            <option value="<?php print_r($get_citys['city']); ?>"><?php print_r($get_citys['city']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>

                                <div class="form__field col-12 col-lg-6"><select name="enq_select_service" class="form__input" id="enqSelectService" required disabled="disabled">
                                        <option value="" disabled="disabled" selected="selected">Select Service</option>
                                        <?php
                                        $CI = &get_instance();
                                        $ger_services = $CI->Common_model->get_data_from_table('services');
                                        ?>
                                        <?php foreach ($ger_services as $get_service) : ?>
                                            <option data-id="<?php print_r($get_service['id']); ?>" value="<?php print_r($get_service['name']); ?>"><?php print_r($get_service['name']); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form__field col-12"><textarea name="enq_message" class="form__input" placeholder="Message(optional)"></textarea></div>
                                <div class="text-center"><button type="submit" class="btn btn-primary">Submit</button></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <a href="javascript:void(0)" class="scroll-to-top-button rounded-circle"><i class="fa-solid fa-chevron-up"></i></a>
    </footer>
    <script>
        (function() {
            var options = {
                whatsapp: "+91 9365336191", // WhatsApp number
                call_to_action: "", // Call to action
                button_color: "#FF6550", // Color of button
                position: "left", // Position may be 'right' or 'left'
            };
            var proto = 'https:',
                host = "getbutton.io",
                url = proto + '//static.' + host;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url + '/widget-send-button/js/init.js';
            s.onload = function() {
                WhWidgetSendButton.init(host, proto, options);
            };
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        })();
    </script>
    </body>

    </html>