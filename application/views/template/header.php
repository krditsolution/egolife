<?php
$CI = &get_instance();
$ger_services = $CI->Common_model->get_data_from_table('services');
if ($this->session->userdata('user_id') > 0) {
    $ger_user_details = $CI->Common_model->get_data_from_table_by_id('users', $this->session->userdata('user_id'));
}
// echo "<pre>";
// print_r($ger_services);exit;
?>

<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url() ?>assets/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url() ?>assets/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url() ?>assets/images/favicon-16x16.png">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:ital,wght@0,300;0,400;0,600;0,700;1,400;1,600;1,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
    <title>Egolife Capital | Home</title>
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script defer="defer" src="<?php echo base_url() ?>assets/js/bundle.js"></script>
    <link href="<?php echo base_url() ?>assets/css/main.css" rel="stylesheet">
</head>

<style>
    .line-clamp {
        display: -webkit-box;
        -webkit-box-orient: vertical;
        overflow: hidden;
    }
    .line-clamp-1{
        -webkit-line-clamp: 1;
    }
    .line-clamp-2{
        -webkit-line-clamp: 2;
    }
    .line-clamp-3{
        -webkit-line-clamp: 3;
    }
</style>

<body>
    <header class="site-header" id="siteHeader">
        <div class="site-header__top bg-light py-2">
            <div class="container">
                <div class="d-flex justify-content-end justify-content-lg-between">
                    <div class="contact-info-wrapper d-none d-lg-flex align-items-center w-auto"><a href="tel:+91 9365336191" class="dark-link fw-semibold pe-3 border-dark border-end">+91 9365336191</a><a href="mailto:info.egolifecapital.in" class="dark-link fw-semibold ps-3">info.egolifecapital.in</a></div>
                    <nav class="top-nav">
                        <ul class="top-nav__menu d-flex justify-content-end">
                            <li class="me-3 d-none d-lg-inline-block"><a href="<?php echo base_url('faq')?>" class="dark-link fw-medium">FAQs</a>
                            </li>
                            <li class="me-3 d-none d-lg-inline-block"><a href="#" class="dark-link fw-medium">Offers</a>
                            </li>
                            <li class="d-inline-block">
                                <a href="<?php echo base_url('login') ?>" class="btn btn-dark px-2 py-1 fw-medium text-capitalize <?php if (!empty($this->session->userdata('user_id'))) {echo "d-none";} ?>">
                                                                                                                                            <span class="me-2">
                                        <i class="fa-regular fa-user"></i><span><span>Login
                                    </span>
                                </a>
                                <div class="dropdown user-profile <?php if (!empty($this->session->userdata('user_id'))) {
                                                                        echo "d-block";
                                                                    } else {
                                                                        echo 'd-none';
                                                                    } ?>">
                                    <button class="dropdown-toggle border-0 d-inline-block bg-transparent" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                        <div class="user-profile-container">
                                            <div class="user-profile__prefix me-2 bg-dark d-flex align-items-center justify-content-center rounded-circle text-white">
                                                <?php echo substr($ger_user_details[0]['name'], 0, 1); ?></div><span><i class="fa-solid fa-caret-down icon"></i></span>
                                        </div>
                                    </button>
                                    <ul class="dropdown-menu border-0 shadow">
                                        <li><a class="dropdown-item" href="#">My Account</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url('user/logout') ?>">Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="site-header__bottom bg-white">
            <div class="container">
                <div class="site-header__inner-wrapper d-flex justify-content-between align-items-center">
                    <div class="d-flex align-items-center"><a href="<?php echo base_url() ?>" class="d-inline-block site-header__logo me-4"><img src="<?php echo base_url() ?>assets/images/logo.png" alt="logo" width="180" height="71" /></a></div>
                    <nav class="site-header__nav w-auto d-none d-lg-inline-flex align-items-center">
                        <ul class="d-flex justify-content-between align-items-center">
                            <li class="active"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li><a href="<?php echo base_url('about-us') ?>">About Us</a></li>
                            <li><a href="#">Services<span class="dropdown-icon ms-2"><i class="fa-solid fa-chevron-down"></i></span></a>
                                <ul class="dropdown-submenu">
                                    <?php foreach ($ger_services as $get_service) : ?>
                                        <?php if (!empty($get_service['url'])) : ?>
                                            <li><a target="_blank" href="<?php echo $get_service['url'] ?>"><?php print_r($get_service['name']); ?></a>
                                            </li>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url('softwares') ?>">Softwares</a></li>
                            <li><a href="<?php echo base_url('products') ?>">Products</a></li>
                            <li><a href="<?php echo base_url('contact-us') ?>">Contact Us</a></li>
                        </ul>
                        <div class="d-inline-block"><a style="padding: 10px 10px;" href="#" class="btn btn-primary" class="site-header__btn--request-a-quote" data-bs-toggle="modal" data-bs-target="#reqAQuoteModal">Request A Quote</a></div>
                    </nav>
                    <ul class="d-flex align-items-center site-header__cta-wrapper d-lg-none">
                        <li class="me-2"><a href="#" class="btn btn-primary" class="site-header__btn--request-a-quote" data-bs-toggle="modal" data-bs-target="#reqAQuoteModal">Request A Quote</a></li>
                        <li><a class="site-header__btn--nav-toggler d-lg-none" data-bs-toggle="offcanvas" href="#offcanvasMobileNav" role="button" aria-controls="offcanvasMobileNav"><i class="bi bi-list"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="offcanvas offcanvas-end bg-white" tabindex="-1" id="offcanvasMobileNav" aria-labelledby="offcanvasMobileNav">
            <div class="offcanvas-header align-items-center">
                <div><a href="#" class="offcanvas__logo"><img src="<?php echo base_url() ?>assets/images/logo.png" alt="logo" width="90" height="35"></a></div><button type="button" class="btn-close text-reset bg-transparent border-0 p-2 text-white h5" data-bs-dismiss="offcanvas" aria-label="Close"></button>
            </div>
            <div class="offcanvas-body">
                <div class="dropdown mt-3">
                    <nav class="site-header__mobile-nav">
                        <ul class="mobile-nav">
                            <li class="active"><a href="<?php echo base_url() ?>">Home</a></li>
                            <li><a href="<?php echo base_url('about-us') ?>">About Us</a></li>
                            <li><a href="#" class="has-dropdown">Services<span><i class="fa-solid fa-plus"></i></span></a>
                                <ul class="dropdown-submenu">
                                    <?php foreach ($ger_services as $get_service) : ?>
                                        <?php if (!empty($get_service['url'])) : ?>
                                            <li><a target="_blank" href="<?php echo $get_service['url'] ?>"><?php print_r($get_service['name']); ?></a>
                                            </li>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url('softwares') ?>">Softwares</a></li>
                            <li><a href="<?php echo base_url('products') ?>">Products</a></li>
                            <li><a href="<?php echo base_url('contact-us') ?>">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <script>
        <?php
        $config = array(
            'base_url' => base_url()
        );
        echo 'var global_config=' . json_encode($config) . ';';
        ?>
    </script>