<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* 
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
// $route['product-listing/(:any)'] = 'Home/product_listing/$1';
$route['about-us'] = 'Home/about';
$route['contact-us'] = 'Home/contact';

$route['login'] = 'User/login';
$route['register'] = 'User/register';
$route['my-profile'] = 'User/my_dashboard';
$route['softwares'] = 'Home/software';
$route['services'] = 'Services/index';

//static
$route['privacy-policy'] = 'Home/privacy_policy';
$route['return-policy'] = 'Home/return_policy';
$route['terms-condition'] = 'Home/terms_of_service';
$route['warranty-policy'] = 'Home/warranty_policy';
$route['offer-zone'] = 'Home/offer_zone';
$route['faq'] = 'Home/faq';

$route['product'] = 'Product/index_2';
$route['checkout'] = 'Cart/checkout';
$route['category/(:any)'] = 'Product/product_by_category/$1';
$route['order-details/(:any)'] = 'user/order_details/$1';
// $route['product/(:any)'] = 'Product/products/$1';
// $route['product/(:any)/(:any)'] = 'Product/products/$1/$1';

$route['product-details/(:any)'] = 'Product/product_display/$1';
// $route['add-to-cart']['post'] = 'Home/addToCart';
// $route['translate_uri_dashes'] = FALSE;
