<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_product extends MY_Controller {
    function __construct() {
        parent::__construct();
        $method =  $this->router->fetch_method();
        $allowed = array('login');
        if(!in_array($method, $allowed)){
            $this->login_check();
        }
    }

    public function login_check(){
        if (!$this->session->userdata('admin_user') || !$this->session->userdata('admin_id')) {
            return redirect(base_url('admin/login'));
        }
        if ($this->session->userdata('user') == "Admin") {
            $is_admin = 1;
        }
    }

    public function product(){
        $products = $this->Product_model->get_all_products('products');
        $data['data'] = array('products' => $products);
		$this->load->view('admin/product/index', $data);
    }

    public function category(){
        $product_category = $this->Common_model->get_data_from_table('product_category');
        $data['data'] = array('product_category' => $product_category);
        $this->load->view('admin/category/index', $data);
    }

    public function delete_category($id){
        $this->Common_model->delete_data_from_table('product_category', $id);
        return redirect('admin_product/category');
    }

    public function edit_category($id){
        if ($this->input->post()) {
            $this->Common_model->update_table('product_category', $id, $this->input->post());
            return redirect('admin_product/category');
        }
        $get_category_by_id = $this->Common_model->get_data_from_table_by_id('product_category', $id);
        $data['data'] = array('category_by_id' => $get_category_by_id);
        $this->load->view('admin/category/edit', $data);
    }

    public function add_product(){
        $get_colors = $this->Common_model->get_data_from_table('color'); 
        $get_sizes = $this->Common_model->get_data_from_table('size'); 
        $get_places = $this->Common_model->get_data_from_table('places'); 
        $get_categories = $this->Common_model->get_data_from_table('product_category');
        $get_tags = $this->Common_model->get_data_from_table('tag');
        $data_for_view['data'] = array('get_colors' => $get_colors, 'get_sizes' => $get_sizes, 'get_places' => $get_places, 'get_categories' => $get_categories,'get_tags' => $get_tags);
        if($this->input->post()){
            $data['name'] = $this->input->post('name');
            $data['product_title'] = $this->input->post('product_title');
            $data['supplier_id'] = $this->input->post('supplier_id');
            $data['mrp'] = $this->input->post('mrp');
            $data['selling_price'] = $this->input->post('selling_price');
            $data['size'] = json_encode($this->input->post('size'));
            $data['color'] = json_encode($this->input->post('color'));
            $data['tag'] = json_encode($this->input->post('tag'));
            $data['brand'] = $this->input->post('brand');
            $data['category'] = $this->input->post('category');
            $data['qty'] = $this->input->post('qty');
            $data['product_description'] = $this->input->post('product_description');
            $data['features'] = $this->input->post('features');
            $data['status'] = 1;
    
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++){           
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
    
                $this->upload->initialize($this->image_config());
                $this->upload->do_upload('userfile');
                $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
            }
            $data['img'] = json_encode($data_media['img']);
            // echo "<pre>";
            // print_r($data);exit();
    
            $this->Common_model->add_data_table('products', $data);
            // echo $this->db->last_query();exit();
            return redirect('admin_product/product');
        }
        $this->load->view('admin/product/add', $data_for_view);
    }

    public function add_product_init(){
        // echo "<per>";
        // print_r($this->input->post());exit();
        $data['name'] = $this->input->post('name');
        $data['product_title'] = $this->input->post('product_title');
        $data['supplier_id'] = $this->input->post('supplier_id');
        $data['mrp'] = $this->input->post('mrp');
        $data['selling_price'] = $this->input->post('selling_price');
        $data['size'] = json_encode($this->input->post('size'));
        $data['color'] = json_encode($this->input->post('color'));
        $data['brand'] = $this->input->post('brand');
        $data['category'] = $this->input->post('category');
        $data['qty'] = $this->input->post('qty');
        $data['product_description'] = $this->input->post('product_description');
        $data['features'] = $this->input->post('features');

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);
        for($i=0; $i<$cpt; $i++){           
            $_FILES['userfile']['name']= $files['userfile']['name'][$i];
            $_FILES['userfile']['type']= $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error']= $files['userfile']['error'][$i];
            $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

            $this->upload->initialize($this->image_config());
            $this->upload->do_upload('userfile');
            $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
        }
        $data['img'] = json_encode($data_media['img']);

        $this->Common_model->add_data_table('products', $data);
        return redirect('admin_product/product');
    }

    public function delete_product($id){
        $this->Common_model->delete_data_from_table('products', $id);
        return redirect('admin_product/product');
    }

    public function edit_product($id){
        $get_colors = $this->Common_model->get_data_from_table('color'); 
        $get_sizes = $this->Common_model->get_data_from_table('size'); 
        $get_places = $this->Common_model->get_data_from_table('places'); 
        $get_categories = $this->Common_model->get_data_from_table('product_category');
        $product_id = $this->Common_model->get_data_from_table_by_id('products', $id);
        $get_tags = $this->Common_model->get_data_from_table_by_id('tag');
        $data['data'] = array('product' => $product_id,'get_colors' => $get_colors, 'get_sizes' => $get_sizes, 'get_places' => $get_places, 'get_categories' => $get_categories, 'get_tags' => $get_tags);
        $this->load->view('admin/product/edit', $data);
    }

    public function do_edit_product(){
        if ($this->input->post()){
            $data['name'] = $this->input->post('name');
            $data['product_title'] = $this->input->post('product_title');
            $data['supplier_id'] = $this->input->post('supplier_id');
            $data['mrp'] = $this->input->post('mrp');
            $data['selling_price'] = $this->input->post('selling_price');
            if(!empty($this->input->post('size'))) {
                $data['size'] = json_encode($this->input->post('size'));
            }
            if(!empty($this->input->post('color'))) {
                $data['color'] = json_encode($this->input->post('color'));
            }
            if(!empty($this->input->post('tag'))) {
                $data['tag'] = json_encode($this->input->post('tag'));
            }
            
            $data['brand'] = $this->input->post('brand');
            $data['category'] = $this->input->post('category');
            $data['qty'] = $this->input->post('qty');
            $data['product_description'] = $this->input->post('product_description');
            $data['features'] = $this->input->post('features');

			$files = $_FILES;
            if (!empty($_FILES['userfile']['name'][0])) {
                $cpt = count($_FILES['userfile']['name']);
                for($i=0; $i<$cpt; $i++){           
                    $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                    $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                    $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
    
                    $this->upload->initialize($this->image_config());
                    $this->upload->do_upload('userfile');
                    $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
                }
                $data['img'] = json_encode($data_media['img']);
            }
            // echo "<pre><br><br>";
            // print_r($data);
            // exit();
            $this->Common_model->update_table('products', $this->input->post('id'), $data);
            return redirect('admin_product/product');
        }
    }

    public function add_category(){
        if ($this->input->post()) {
            $config = ['upload_path' => './assets/images/category/', 'allowed_types' => 'gif|jpg|jpeg|png', 'height'=>'150px', 'width'=>'150px', 'remove_spaces' => true, 'encrypt_name' => true];
				$this->load->library('upload', $config);
				$this->upload->do_upload('userfile');
				$dataInfo = $this->upload->data();
                
				$add_user_data = array();
				$add_user_data['name'] = $this->input->post('name');
				$add_user_data['img'] = $dataInfo['file_name'];                
            $this->Common_model->add_data_table('product_category', $add_user_data);
            return redirect('admin_product/category');
        }
        $this->load->view('admin/category/add');
    }



    private function image_config(){
        $config = array();
        $config['upload_path'] = './assets/images/products/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg|svg|webp';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;
        $config['remove_spaces'] = true;
        $config['encrypt_name'] = true;
        return $config;
    }
}

?>