<?php 
    class User extends MY_Controller{

        public function login_check(){
			if (!$this->session->userdata('site_user') || !$this->session->userdata('user_id')) {
				 return redirect(base_url('login'));
			}
		}

        public function login(){
            if ($this->session->userdata('user_id')) {
                return redirect(base_url());
            }else{
                $this->load->view('frontend/user/login');
            }
        }

        public function login_user(){
            $data['email_addr'] = $this->input->post('email_addr');
            $data['password'] = md5($this->input->post('password'));
            
            $user_data = $this->User_model->login_user($data);
            // echo $this->db->last_query();
            // print_r($user_data);
            // exit();
            if($user_data['user_id'] > 0){
                $this->session->set_userdata('user_type',$user_data['user_type']);
                $this->session->set_userdata('user_id', $user_data['user_id']);
                return redirect(base_url());
            }else{
                $this->session->set_flashdata('Login_failed','Invalid Username/Password');
                return redirect('login');
            }
        }

        public function register(){
            $this->load->view('frontend/user/login');
        }

        public function user_register(){
            $this->form_validation->set_rules('name', "Full Name", 'required');
            $this->form_validation->set_rules('email_addr', "Email Id", 'required|valid_email');
            $this->form_validation->set_rules('mobile_number', "Mobile Number", 'required|is_unique[users.mobile_number]|numeric|min_length[10]|max_length[10]');
            $this->form_validation->set_rules('password', "Password", 'required');

            if($this->check_user($this->input->post('email_addr')) > 0){
                 echo '<script>alert("The email id is already Registered! Redirect to Login");window.location.href = " '.base_url('login').'";</script>';
            }else{
                $data['name'] = $this->input->post('name');
                $data['email_addr'] = $this->input->post('email_addr');
                $data['mobile_number'] = $this->input->post('mobile_number');
                $data['user_type'] = "3";
                $data['password'] = md5($this->input->post('password'));
                $data['status'] = 1;

                // $config = ['upload_path' => './assets/images/user/', 'allowed_types' => 'gif|jpg|jpeg|png', 'height'=>'150px', 'width'=>'150px', 'remove_spaces' => true, 'encrypt_name' => true];
                // $this->load->library('upload', $config);
                // $this->upload->do_upload('image');

                // $data['img'] = $this->upload->data()['file_name'];

                $this->Common_model->add_data_table('users',$data);
                return redirect('login');
            }
        }

        private function check_user($email_id){
            $is_exit = $this->User_model->check_exits_user($email_id);
            return $is_exit;
        }

        public function wishlist(){
            $this->login_check();
            $user_id = $this->session->userdata('user_id');
            $data['get_wishlist'] = array('get_wishlist'=>$this->User_model->get_data_from_wishlist_by_user_id($user_id));
            $this->load->view('frontend/user/wishlist', $data);
        }

        public function cart(){
            $this->load->view('frontend/user/cart');
        }

        public function logout(){
            if($this->session->userdata('user_type') || $this->session->userdata('user_id')){
                $this->session->unset_userdata('user_id');
                $this->session->unset_userdata('site_user');
                $this->session->unset_userdata('user_type');
                //$this->session->sess_destroy();
                return redirect(base_url('login'));
            }
        }

        public function my_dashboard(){
            $this->login_check();
            $user_id = $this->session->userdata('user_id');
            $all_cart_item = $this->User_model->get_data_from_cart_by_user_id($user_id);
            $all_wishlist_item = $this->User_model->get_data_from_wishlist_by_user_id($user_id);
            $get_user_address_by_id = $this->User_model->userAddress($user_id);
            $get_orders_by_user_id = $this->User_model->allorder($user_id);

            $data['data'] = array('user_details' => $this->User_model->get_user_by_id($user_id), 'all_cart_item' => $all_cart_item, 'all_wishlist_item' => $all_wishlist_item, 'get_user_address_by_id' => $get_user_address_by_id,'get_orders_by_user_id' => $get_orders_by_user_id);
            $this->load->view('frontend/user/my_dashboard', $data);
        }

        public function user_add_address(){
            $this->login_check();
            $data['address'] = $this->input->post('address').', '.$this->input->post('door_address');
            $data['zip_code'] = $this->input->post('zip_code');
            $data['state'] = $this->input->post('state');
            $data['city'] = $this->input->post('city');
            $data['user_id'] = $this->session->userdata('user_id');
            $data['landmark'] = $this->input->post('landmark');
            $is_default = $this->User_model->check_default_address($this->session->userdata('user_id'));
            if($is_default > 0){
                $data['is_default'] = '0'; 
            }else{
                $data['is_default'] = '1';
            }
            $this->User_model->add_address_by_user($data);
            $get_last_address_id = $this->User_model->get_last_address_id($this->session->userdata('user_id'));
            $update_address['address'] = $get_last_address_id[0]['id'];
            $this->User_model->update_address_in_user_table($this->session->userdata('user_id'), $update_address); 
            return redirect($this->input->post('origin_url'));           
        }

        public function make_default_address($address_id){
            $this->login_check();
            $user_id = $this->session->userdata('user_id');
            $this->User_model->removeDefaultAddress($address_id, $user_id);
            $this->User_model->addDefaultAddress($address_id, $user_id);
            return redirect(base_url('checkout'));        
        }

        public function update_wishlist(){
            $this->login_check();
            $check_fevorite = $this->User_model->check_wishlist_by_user_id($this->input->post('product_id'),$this->input->post('user_id'));
            if ($check_fevorite > 0) {
                echo "Product Already Added";
            }else{
                $this->Common_model->add_data_table('fevorite', $this->input->post());
            }
        }

        public function delete_wishlist($id){
            $this->login_check();
            $this->User_model->deleteWishProduct($id);
            return redirect('./wishlist');
        }

        public function delete_all_wishlist(){
            $this->login_check();
            $this->User_model->deleteAllWishProduct($this->session->userdata('user_id'));
            //echo $this->db->last_query();
            return redirect('./wishlist');
        }

        public function order_details($order_id){
            $this->login_check();
            $user_id = $this->session->userdata('user_id');
            $order_details = $this->User_model->getAllUserOrders($order_id, $user_id);
            $data['data'] = array('order_details' => $order_details);
            $this->load->view('frontend/user/order_details', $data);
        }

        public function news_letter_email(){
            $data['email_addr'] = $this->input->post('email');
            $this->Common_model->add_data_table('news_letter', $data);
            $this->email_send($from_email = 'admin@thinksurfmedia.com', $to_email = $data['email_addr'], $replay_to = $data['email_addr'], $email_cc = '', $email_subject = "One Order has been placed successfully", $email_body = newsletter_email($data['email_addr']));
        }

        private function email_send($from_email = null, $to_email = null, $replay_to = null, $email_cc = null, $email_subject = null, $email_body = null){
            $this->email->set_mailtype("html");
            $this->email->from($from_email, 'Viva Impex'); 
            $this->email->to($to_email);
            $this->email->cc($email_cc);
            $this->email->subject($email_subject); 
            $this->email->message($email_body); 
    
            if($this->email->send()){
                return true;
            }else{
                return false;
            }
        }

        public function session(){
            echo "<pre>";
            print_r($this->session->userdata());
        }

    }
?>