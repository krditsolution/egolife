<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
    function __construct() {
        parent::__construct();
        $method =  $this->router->fetch_method();
        $allowed = array('login');
        if(!in_array($method, $allowed)){
            $this->login_check();
        }
    }

    public function login_check(){
        if(empty($this->session->userdata('admin_id'))) {
            return redirect(base_url('admin/login'));
        }
    }
    
    public function login(){
        if ($this->session->userdata('admin_id')) {
            return redirect("admin");
        }else{
            $this->form_validation->set_rules('uname','User Name','required|valid_email');
            $this->form_validation->set_rules('password','Password','required');
            if($this->form_validation->run()){
                $data['email_addr'] = $this->input->post('uname');
                $data['password'] = md5($this->input->post('password'));
                $data['user_type'] = false;
                $login = $this->Admin_model->admin_login($data);
                // echo $this->db->last_query();
                // print_r($login);
                // exit();
                if($login){
                    $this->session->set_userdata('admin_user',$login['user_type']);
                    $this->session->set_userdata('admin_id', $login['user_id']);
                    return redirect('admin');
                }else{
                    $this->session->set_flashdata('Login_failed','Invalid Username/Password');
                    return redirect(base_url('admin/login'));
                }
            }else{
                $this->form_validation->set_error_delimiters('<div class="text-danger">','</div>');
                $this->load->view('admin/signin');
            }
        }
    }
    
    public function index(){
        // $this->login_check();
        $this->load->view("admin/index");
    }

    public function logout(){
        if($this->session->userdata('admin_id')){
            $this->session->unset_userdata('admin_user');
            $this->session->unset_userdata('admin_id');
            return redirect(base_url('admin/login'));
        }else{
            return redirect('admin/login');
        }
    }
}

?>