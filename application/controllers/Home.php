<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
		// function __construct() {
		// 	parent::__construct();
		// 	$method=  $this->router->fetch_method();
		// 	$allowed=array('index');
		// 	if(!in_array($method,$allowed)){
		// 		$this->login_check();
		// 	}
		// }

	public function index(){
		$data['data'] = array('services'=> $this->Common_model->get_data_from_table('services'));
		$this->load->view('frontend/home', $data);
	}

	public function about(){
		//$data['data'] = array('get_all_products' => $this->Product_model->get_all_product_15());
		$this->load->view('frontend/about');
	}

	public function contact(){
		$this->load->view('frontend/contact');
	}

	public function privacy_policy(){
		$this->load->view('frontend/static/policy');
	}

	public function return_policy(){
		$this->load->view('frontend/static/return_policy');
	}

	public function terms_of_service(){
		$this->load->view('frontend/static/trams_services');
	}

	public function warranty_policy(){
		$this->load->view('frontend/static/warranty_policy');
	}

	public function search(){
		return redirect(base_url('product-details/'.$this->input->post('product_search')));
	}

	public function offer_zone(){
		$this->load->view('frontend/offer-zone');
	}

	public function edit_Personal_details(){
		$data = $this->input->post();
		$this->Common_model->update_table('users', $this->input->post('id'), $data);
		return redirect(base_url('my-profile'));
	}

	public function software(){
		$data['data'] = array('softwars' => $this->Common_model->get_data_from_table('softwars'));
		$this->load->view('frontend/products/product', $data);
	}

	public function faq(){
		$this->load->view('frontend/faq');
	}


	
}
?>
