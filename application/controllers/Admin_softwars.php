<?php defined('BASEPATH') OR exit('No direct script access allowed');
    class Admin_softwars extends MY_Controller {
        public function index(){
            $data['data'] = array('get_softwars' => $this->Common_model->get_data_from_table('softwars'));
            $this->load->view('admin/softwars/index', $data);
        }

        public function add(){
            $this->load->view('admin/softwars/add');
        }

        public function add_software_init(){
            $data['name'] = $this->input->post('name');
            $data['mrp'] = $this->input->post('mrp');
            $data['selling_price'] = $this->input->post('selling_price');
            $data['url'] = $this->input->post('url');

            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++){           
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

                $this->upload->initialize($this->image_config());
                $this->upload->do_upload('userfile');
                $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
            }
            $data['img'] = json_encode($data_media['img']);

            $this->Common_model->add_data_table('softwars', $data);
            return redirect('Admin_softwars/index');
        }

        public function delete_software($id){
            $this->Common_model->delete_data_from_table('softwars', $id);
            return redirect('admin_softwars');
        }

        public function edit_software($id){
            $product_id = $this->Common_model->get_data_from_table_by_id('softwars', $id);
            $data['data'] = array('product' => $product_id);
            $this->load->view('admin/softwars/edit', $data);
        }
    
        public function do_edit_software(){
            if ($this->input->post()){
                $data['name'] = $this->input->post('name');
                $data['mrp'] = $this->input->post('mrp');
                $data['selling_price'] = $this->input->post('selling_price');
                $data['url'] = $this->input->post('url');
    
                $files = $_FILES;
                if (!empty($_FILES['userfile']['name'][0])) {
                    $cpt = count($_FILES['userfile']['name']);
                    for($i=0; $i<$cpt; $i++){           
                        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
        
                        $this->upload->initialize($this->image_config());
                        $this->upload->do_upload('userfile');
                        $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
                    }
                    $data['img'] = json_encode($data_media['img']);
                }

                $this->Common_model->update_table('softwars', $this->input->post('id'), $data);
                return redirect('admin_softwars');
            }
        }

        private function image_config(){
            $config = array();
            $config['upload_path'] = './assets/images/software/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|svg|webp';
            $config['max_size']      = '0';
            $config['overwrite']     = FALSE;
            $config['remove_spaces'] = true;
            $config['encrypt_name'] = true;
            return $config;
        }
    }
?>
