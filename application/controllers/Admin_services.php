<?php defined('BASEPATH') OR exit('No direct script access allowed');
    class Admin_services extends MY_Controller {
        public function index(){
            $data['data'] = array('services' => $this->Common_model->get_data_from_table('services'));
            $this->load->view('admin/services/index', $data);
        }

        public function add(){
            $this->load->view('admin/services/add');
        }

        public function add_services_init(){
            $data['name'] = $this->input->post('name');
            $data['description'] = $this->input->post('description');
            $data['url'] = $this->input->post('url');

            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for($i=0; $i<$cpt; $i++){           
                $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i];    

                $this->upload->initialize($this->image_config());
                $this->upload->do_upload('userfile');
                $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
            }
            $data['img'] = json_encode($data_media['img']);

            $this->Common_model->add_data_table('services', $data);
            return redirect('Admin_services/index');
        }

        public function delete_service($id){
            $this->Common_model->delete_data_from_table('services', $id);
            return redirect('admin_services');
        }

        public function edit_service($id){
            $product_id = $this->Common_model->get_data_from_table_by_id('services', $id);
            $data['data'] = array('product' => $product_id);
            $this->load->view('admin/services/edit', $data);
        }
    
        public function do_edit_services(){
            if ($this->input->post()){
                $data['name'] = $this->input->post('name');
                $data['description'] = $this->input->post('description');
                $data['url'] = $this->input->post('url');

                $files = $_FILES;
                if (!empty($_FILES['userfile']['name'][0])) {
                    $cpt = count($_FILES['userfile']['name']);
                    for($i=0; $i<$cpt; $i++){           
                        $_FILES['userfile']['name']= $files['userfile']['name'][$i];
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i];    
        
                        $this->upload->initialize($this->image_config());
                        $this->upload->do_upload('userfile');
                        $data_media['img']['images_'.$i] = $this->upload->data()['file_name'];
                    }
                    $data['img'] = json_encode($data_media['img']);
                }

                $this->Common_model->update_table('services', $this->input->post('id'), $data);
                return redirect('admin_services');
            }
        }

        private function image_config(){
            $config = array();
            $config['upload_path'] = './assets/images/services/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg|svg|webp';
            $config['max_size']      = '0';
            $config['overwrite']     = FALSE;
            $config['remove_spaces'] = true;
            $config['encrypt_name'] = true;
            return $config;
        }
    }
?>
