<?php 
    require APPPATH.'views/razorpay-php-master/Razorpay.php';
    use Razorpay\Api\Api;
    class Payment extends MY_Controller{

        public function login_check(){
			if (!$this->session->userdata('site_user') || !$this->session->userdata('user_id')) {
				 return redirect(base_url('login'));
			}
		}

        public function index(){
            $this->login_check();
            $key_id = 'rzp_test_4iVGULDvrQIe8S';
            $secret = 'K6UrfKN8yOybDuotCipUMAQZ';

            foreach ($this->cart->contents() as $cart_ses) {
                $subtotal[] = $cart_ses['subtotal'];
                $product_details[] = array('product_id' => $cart_ses['id'],'product_qty' => $cart_ses['qty'], 'product_color' => $cart_ses['color'], 'product_size' => $cart_ses['size'], 'place' => $cart_ses['place'],'price'=> $cart_ses['subtotal']);
            }
            $product_subtotal = array_sum($subtotal);
            $product_tax = $product_subtotal * vivaTaxCalculator();
            $product_shipping = vivaShippingCharges();
            $product_grandtotal = $product_subtotal+$product_tax+$product_shipping;
            $product_data = array('grandtotal' =>  $product_grandtotal, 'product_details' => $product_details);


            $api = new Api($key_id, $secret);
            $orderData = [
                'receipt'         => 'rcptid_11',
                'amount'          => number_format($product_data['grandtotal'], 2, '.', '')*100,
                'currency'        => 'INR'
            ];
            $razorpayOrder = $api->order->create($orderData);
            $address = $this->User_model->get_default_address_by_user_id($this->session->userdata('user_id'));

            $data['data'] = array('address'=>$address, 'product_data' => $product_data, 'user_id' => $this->session->userdata('user_id'), 'order_data' => $razorpayOrder, 'key_id' => $key_id, 'secret' => $secret);

            $order_data = array('address_id' => $address[0]['id'], 'product_data' => $product_data['product_details'], 'total_amount' => number_format($product_data['grandtotal'], 2, '.', '')*100);
            $this->session->set_userdata('order_data', $order_data);
            $this->load->view('frontend/user/payment', $data);
        }

        public function success(){
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $razorpay_order_id = $this->input->post('razorpay_order_id');
            $razorpay_signature = $this->input->post('razorpay_signature');
            $secret = 'K6UrfKN8yOybDuotCipUMAQZ';
            $data = $razorpay_order_id."|" .$razorpay_payment_id;
            $generated_signature = hash_hmac('sha256', $data, $secret);
            if ($generated_signature == $razorpay_signature) {

                // $get_last_order = $this->User_model->get_last_order_from_table();
                $get_shipping_address = $this->Common_model->get_data_from_table_by_id('address', $this->session->userdata('order_data')['address_id']);
                $product_details = $this->session->userdata('order_data')['product_data'];

                //Get User Data
                $get_user_data = $this->Common_model->get_data_from_table_by_id('users', $this->session->userdata('user_id'));

                foreach ($this->session->userdata('order_data')['product_data'] as $key => $order_data_session) {
                    $order_data['product'] = json_encode($order_data_session);
                    $order_data['user_id'] = $this->session->userdata('user_id');
                    $order_data['total_price'] = $order_data_session['price'] * $order_data_session['product_qty'];
                    $order_data['payment_status'] = "Paid";
                    $order_data['order_status'] = "Pending";
                    $order_data['place'] = $order_data_session['place'];
                    $order_data['address_id'] = $this->session->userdata('order_data')['address_id'];
                    $this->User_model->insert_order_table('orders', $order_data);

                    $get_last_order = $this->User_model->get_last_order_from_table();

                    //Add order into the Ship Rocket
                    $get_product_data = $this->Common_model->get_data_from_table_by_id('products', $order_data_session['product_id']);
                    $product_qty = $order_data_session['product_qty'];
                    $product_size = $order_data_session['product_size'];
                    $product_color = $order_data_session['product_color'];
                    $product_price = $order_data_session['price'];                    

                    place_order_in_ship_rocket($get_user_data, $get_product_data, $product_qty, $product_size, $product_color, $product_price, $get_shipping_address, $get_last_order);
                }
                //Get User Data
                //$get_user_data = $this->Common_model->get_data_from_table_by_id('users', $this->session->userdata('user_id'));

                //Get product data

                //Send Email 
                // $this->email_send($from_email = 'admin@thinksurfmedia.com', $to_email = 'kazi@thinksurfmedia.com', $replay_to = $get_user_data['email_addr'], $email_cc = $get_user_data['email_addr'], $email_subject = "One Order has been placed successfully", $email_body = order_confirm_email($email_data));

                //Add order into the Ship Rocket

                // $get_last_order = $this->User_model->get_last_order_from_table();
                // $get_shipping_address = $this->Common_model->get_data_from_table_by_id('address', $this->session->userdata('order_data')['address_id']);
                // $product_details = $this->session->userdata('order_data')['product_data'];

                // for ($i=0; $i < count($product_details); $i++) {
                //     $get_product_data = $this->Common_model->get_data_from_table_by_id('products', $product_details[$i]['product_id']);
                //     $product_qty = $product_details[$i]['product_qty'];
                //     $product_size = $product_details[$i]['product_size'];
                //     $product_color = $product_details[$i]['product_color'];
                //     $product_price = $product_details[$i]['price'];

                //     place_order_in_ship_rocket($get_user_data, $get_product_data, $product_qty, $product_size, $product_color, $product_price, $get_shipping_address, $get_last_order);
                // }
                
                $this->cart->destroy();
                $this->session->unset_userdata('order_data');
                return redirect(base_url('my-profile'));
            }
        }

        private function email_send($from_email = null, $to_email = null, $replay_to = null, $email_cc = null, $email_subject = null, $email_body = null){
            $this->email->set_mailtype("html");
            $this->email->from($from_email, 'Awaken Intuition'); 
            $this->email->to($to_email);
            $this->email->cc($email_cc);
            $this->email->subject($email_subject); 
            $this->email->message($email_body); 
    
            if($this->email->send()){
                return true;
            }else{
                return false;
            }
        }

        public function test_payment_class(){
            // place_order_in_ship_rocket();
            echo "<pre>";
            print_r($this->session->userdata());
        }


    }

?>