<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends MY_Controller {
    public function users(){
        $all_users = $this->Admin_model->get_all_users();
        $data['data'] = array('all_users' => $all_users);
        $this->load->view('admin/user/index', $data);
        // echo "<pre>";
        // print_r($this->session->userdata());
    }

    public function block_unblock_user($id, $status){
        $data['status'] = $status;
        $this->Admin_model->block_unblock_user($id, $data);
        return redirect('admin_user/users');
    }

    public function suppliers(){
        $get_places = $this->Admin_model->get_places();
        $data['data'] = array('get_places' => $get_places);

        $this->load->view('admin/places/index', $data);
    }
}

?>