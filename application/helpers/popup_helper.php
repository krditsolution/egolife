<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

function popup_message($origin_page, $message, $title, $type){
    $output = '';
    $output .= '
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">';

        $output .= '<script>';
        $output .= 'var origin_page ='."'$origin_page'".';';
        $output .= 'var message ='."'$message'".';';
        $output .= 'var title ='."'$title'".';';
        $output .= 'var type ='."'$type'".';';
        $output .= 'swal({
                title: title,
                text: message,
                type: type,
                timer: 2000,
                showConfirmButton: false
                }, function(){
                    window.location.href = origin_page;
                });';
        $output .= '</script>';
        $output .= '
        </body>
        </html>
        ';
        return $output;
}

?>