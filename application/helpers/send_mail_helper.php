<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    function order_confirm_email($data){
        $output = '';
        $output .= '<b><H2 style="text-align:center">Congratulation One course has been ordered successfully</H2></b>
                    <div style="text-align:center">
                        <img style="max-width:100%; height: 100px; margin: 0 auto; display: block;" src="https://awakenintuition.me/assets/images/logo.png">
                    </div>
                    <div style="text-align:center">
                        <table style="border-collapse: collapse;text-align:center;margin: 0 auto; font-family:Roboto,SANS-SERIF;width: 40%;">
                            <tr style="background-color: #ccc;">
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Name:</th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">'.$data['user_name'].'</td>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Email:</th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">'.$data['email_addr'].'</td>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Phone: </th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">'.$data['mobile'].'</td>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Product Name: </th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">'.$data['product_name'].'</td>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Paid Amount: </th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">$'.$data['total_price'].'</td>
                            </tr>
                            <tr>
                                <th style=" border: 1px solid #dddddd;padding: 8px;">Date & Time</th>
                                <td style=" border: 1px solid #dddddd;padding: 8px;">'.date('M-d-D h:ma').'</td>
                            </tr>
                        </table>
                    </div>
                    <p style="font-size:22px; color:green;text-align: center"> To see the Courses go to <b>My Account > My Order</b>';
        return $output;
    }

    function newsletter_email($email){
        $output = "";
        $output .= '
        <div>
        <div style="text-align:center">
        <a href="https://vivabaazar.com">
        <img style="max-width:100%;height:100px;display:inline-block" src="https://vivabaazar.com//assets/images/viva-logo.png">
        </a>
        </div>
        <b><h2 style="text-align:center">Congratulation one news letter</h2></b>
        </div>
        <p style="text-align:center">Thank you for Submit the Email We will notify Soon </p><div class="yj6qo"></div><div class="adL">
       </div></div></div>
        ';
        return $output;
    }
?>