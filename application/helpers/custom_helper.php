<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    function vivaTaxCalculator(){
        $tax = 18/100;
        return $tax;
    }

    function vivaShippingCharges(){
        $shipping = '0';
        return $shipping;
    }
    
    function testimonial(){
        $output = '';
        $output .= '<section class="testimonial-section bg-light section--py">
        <div class="container position-relative">
            <div class="section__header text-center">
                <h2 class="text-white fw-bold text-uppercase">Our Happy Clients</h2>
                <p class="text-muted">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, facilis?</p>
            </div>
            <div class="swiper testimonial-swiper px-lg-3 pb-4">
                <div class="swiper-wrapper pb-4">
                    <div class="swiper-slide">
                        <div class="testimonial-item shadow">
                            <div class="testimonial-item__header">
                                <div class="testimonial-item__profile d-inline-block rounded-circle overflow-hidden"><img src="'.base_url().'assets/images/testimonials/tm-1.png" alt="" width="100" height="100"></div>
                            </div>
                            <div class="testimonial-item__body mt-2">
                                <p class="text-para mb-3">The Egolife Capital is able to provide them with digital solutions effectively. Happy to have worked with them. Well done!</p>
                                <div class="testimonial-item__review-container text-secondary mb-2"><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i></div>
                                <p class="text-dark mb-0 fw-semibold">Bhrigu Deka, Assam</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-item shadow">
                            <div class="testimonial-item__header">
                                <div class="testimonial-item__profile d-inline-block rounded-circle overflow-hidden"><img src="'.base_url().'assets/images/testimonials/tm-2.png" alt="" width="100" height="100"></div>
                            </div>
                            <div class="testimonial-item__body mt-2">
                                <p class="text-para mb-3">We were looking to build our brand and ultimately increase conversions. The Egolife Capital increasing our revenue by 70%.</p>
                                <div class="testimonial-item__review-container text-secondary mb-2"><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i></div>
                                <p class="text-dark mb-0 fw-semibold">MD HANIF AHMED, Assam</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="testimonial-item shadow">
                            <div class="testimonial-item__header">
                                <div class="testimonial-item__profile d-inline-block rounded-circle overflow-hidden"><img src="'.base_url().'assets/images/testimonials/tm-3.png" alt="" width="100" height="100"></div>
                            </div>
                            <div class="testimonial-item__body mt-2">
                                <p class="text-para mb-3">The Egolife Capital series of reports and practices as well as institutional and legal frameworks.</p>
                                <div class="testimonial-item__review-container text-secondary mb-2"><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i><i class="bi bi-star-fill"></i></div>
                                <p class="text-dark mb-0 fw-semibold">Rahul Gupta, Delhi</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>';
    return $output;
    }

    function faq(){
        $output = '';
        $output .= '
        <section class="faqs-home-section bg-light section--pb">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-12 col-lg-8 col-xl-6 mb-4 mb-xl-0"><img src="'.base_url().'assets/images/faqs-img.webp" class="w-100" alt="" width="640" height="640"></div>
                    <div class="col-12 col-lg-8 col-xl-6 pt-2 pt-xl-0">
                        <div class="accordion-container mb-4 ps-xl-5 ps-xxl-4">
                            <h1 class="text-dark fw-bold">Have a <span class="text-primary">question?</span></h1>
                            <p class="text-para mb-4 mb-lg-5">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Natus, autem id veritatis iste ipsa nihil dolorum quas facilis fugiat omnis?</p>
                            <div class="set pt-2"><a href="javascript:void(0)" class="active"><span>How will service providers use Aadhaar Offline e-KYC?</span><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="content" style="display: block;">
                                    <p>The process of Aadhaar Offline e-KYC Verification by Service Provider is:

                                        Once service provider obtains the ZIP file, it extracts the XML file using the password (share code) provided by the resident.
                                        The XML file will contain the demographic details such as Name, DOB, Gender and Address. Photo is in base 64 encoded format which can be rendered directly using any utility or plane HTML page. Email Address and Mobile number are hashed.
                                        Service Provider has to collect Email Address and Mobile number from residents and perform below operations in order to validate the hash:
                                        Mobile Number:
                                        
                                        Hashing logic: Sha256(Sha256(Mobile+ShareCode))*number of times of last digit of Aadhaar Number
                                        
                                        Example :
                                        Mobile number: 9800000002
                                        Aadhaar Number: 123412341234
                                        Share Code: Abc@123
                                        Sha256(Sha256(9800000002+ Abc@123))*4
                                        In case if Aadhaar Number ends with Zero or 1 (123412341230/1) it will be hashed one time.
                                        Sha256(Sha256(9800000002+ Abc@123))*1
                                        
                                        Email Address:
                                        
                                        Hashing Logic: This is a simple SHA256 hash of the email without any salt
                                        
                                        Entire XML is digitally signed and Service Provider can validate the XML file using the signature and public key available on the UIDAI website.(https://uidai.gov.in/images/uidai_offline_publickey_26022019.cer).</p>
                                </div>
                            </div>
                            <div class="set"><a href="javascript:void(0)"><span>What are the rules for PAN card?</span><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="content">
                                    <p>Every person who intends to enter into specified financial transactions in which quoting of PAN is mandatory. Every non-individual resident persons and persons associated with them shall apply for PAN if the financial transaction entered into by them during the financial year exceeds Rs. 2,50,000.</p>
                                </div>
                            </div>
                            <div class="set"><a href="javascript:void(0)"><span>Can I get a PAN card in 2 days?</span><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="content">
                                    <p>After submitting the PAN Card application form, the PAN card is usually issued within 15-20 working days. However, applicants can now obtain their PAN card in 48 Hours (2 days). The procedure is as follows: Visit the NSDL website and choose the relevant form from the list of possibilities.</p>
                                </div>
                            </div>
                            <div class="set"><a href="javascript:void(0)"><span>How to link your Aadhaar to Idea mobile number?</span><i class="fa fa-plus" aria-hidden="true"></i></a>
                                <div class="content">
                                    <p>In case you have a Idea SIM card, you can link your Idea mobile number to Aadhaar is few simple steps. Here`s is a step-by-step guide of the procedure: <br>
                                        1) Visit the nearest Idea center located in your area. <br>
                                        2) Carry your Aadhaar card or photo copy of your Aadhaar or simply not your Aadhaar number and your operational Idea mobile number <br>
                                        3) Give the details of your Aadhaar number and mobile number to the Idea store executive <br>
                                        4) The executive will provide a four digit verification code (OTP) on your registered Idea mobile number for re-verification process <br>
                                        5) You need to provide this verification code to Idea store executive<br> 
                                        6) Next, the executive will verify your biometrics to match with the Aadhaar number <br>
                                        Once details are validated and verified, your Idea mobile number will be linked to Aadhaar within 24 hours. You will also get a notification on your Idea number regarding the linking of Idea number to Aadhaar. </p>
                                </div>
                            </div>
                            <h6 class="text-dark pt-3 lh-lg text-center text-xl-start">Have more questions? <a href="#" class="primary-link text-decoration-underline">Click here</a> to Visit our FAQs page or <a href="#" class="btn btn-primary ms-1 d-inline-block">Get In Touch</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        ';
        return $output;
    }
