<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    function create_token(){
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/auth/login",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>"{\n    \"email\": \"dev@thinksurfmedia.com\",\n    \"password\": \"Dev@2022\"\n}",
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
            ),
        ));
        $SR_login_Response = curl_exec($curl);
        curl_close($curl);
        $SR_login_Response_out = json_decode($SR_login_Response);
        return $token = $SR_login_Response_out->{'token'};
    }

    function place_order_in_ship_rocket($get_user_data = NULL, $get_product_data = NULL, $product_qty = NULL, $product_size = NULL, $product_color = NULL, $product_price = NULL, $get_shipping_address = NULL, $get_last_order = NULL){
        $token = create_token();
        $tax = $product_price + ($product_price*18/100);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apiv2.shiprocket.in/v1/external/orders/create/adhoc",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS =>'{
                "order_id": "'.$get_last_order[0]['id'].'",
                "order_date": "'.$get_last_order[0]['date_time'].'",
                "pickup_location": "Viva pickup Address",
                "billing_customer_name": "'.$get_user_data[0]['name'].'",
                "billing_last_name": "'.$get_user_data[0]['name'].'",
                "billing_address": "'.$get_shipping_address[0]['address'].'",
                "billing_address_2": "'.$get_shipping_address[0]['landmark'].'",
                "billing_city": "'.$get_shipping_address[0]['city'].'",
                "billing_pincode": "'.$get_shipping_address[0]['zip_code'].'",
                "billing_state": "'.$get_shipping_address[0]['state'].'",
                "billing_country": "India",
                "billing_email": "'.$get_user_data[0]['email_addr'].'",
                "billing_phone": "'.$get_user_data[0]['mobile_number'].'",
                "shipping_is_billing": true,
                "shipping_customer_name": "",
                "shipping_last_name": "",
                "shipping_address": "",
                "shipping_address_2": "",
                "shipping_city": "",
                "shipping_pincode": "",
                "shipping_country": "",
                "shipping_state": "",
                "shipping_email": "",
                "shipping_phone": "",
                "order_items": [
                    {
                        "name": "'.$get_product_data[0]['name'].'",
                        "sku": "'.$get_product_data[0]['name'].'",
                        "units": "'.$product_qty.'",
                        "selling_price": "'.$product_price.'",
                        "discount": "",
                        "tax": "'.$tax.'",
                        "hsn": 441122
                    }
                ],
                "payment_method": "Prepaid",
                "shipping_charges": 0,
                "giftwrap_charges": 0,
                "transaction_charges": 0,
                "total_discount": 0,
                "sub_total": "'.$product_qty * $product_price.'",
                "length": 10,
                "breadth": 15,
                "height": 20,
                "weight": 0.5
            }',
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json",
            "Authorization: Bearer $token"
            ),
        ));
        $SR_login_Response = curl_exec($curl);
        curl_close($curl);
        //$SR_login_Response_out = json_decode($SR_login_Response);
        echo '<pre>';
        print_r($SR_login_Response);
    }
    
?>